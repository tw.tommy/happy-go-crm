package com.crm.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.crm.common.HttpStatus;
import com.crm.common.PageResult;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Component
@Slf4j
@Data
public class DataManager {

	private EntityManager entityManager;
	private PageRequest pagable;
	private StringBuilder sql;
	private Map<String, Object> parameters;

	private Query queryData;
	private Query queryCount;
	private Class<?> clazz;

	public DataManager(EntityManager entityManager) {
		this.sql = new StringBuilder();
		this.parameters = new HashMap<String, Object>();
		this.pagable = null;
		this.clazz = null;
		this.entityManager = entityManager;
	}

	public DataManager(EntityManager entityManager, PageRequest pagable) {
		this.sql = new StringBuilder();
		this.parameters = new HashMap<String, Object>();
		this.pagable = pagable;
		this.entityManager = entityManager;
	}

	public DataManager(EntityManager entityManager, Class<?> clazz, PageRequest pagable) {
		this.sql = new StringBuilder();
		this.parameters = new HashMap<String, Object>();
		this.pagable = pagable;
		this.clazz = clazz;
		this.entityManager = entityManager;
	}

	public void setModel(Class<?> clazz) {
		this.clazz = clazz;
	}

	public void setPageRequest(PageRequest pagable) {
		this.pagable = pagable;
	}

	/**
	 * 取得單筆查詢結果
	 */
	public Object getSingleResult() {
		this.initQueryData();
		return this.queryData.getSingleResult();
	}

	/**
	 * 取得查詢結果
	 */
	public List<?> getResult() {
		if (this.getTotal() == 0) {
			return null;
		}
		
		this.initQueryData();
		return this.queryData.getResultList();
	}

	/**
	 * 取得查詢結果(含分頁資訊)
	 */
	public PageResult<?> getPageResult() {
		this.initQueryData();

		if (null != this.pagable) {
			this.queryData.setFirstResult((int) this.pagable.getOffset());
			this.queryData.setMaxResults(this.pagable.getPageSize());
		}

		PageResult<List<?>> pageResult = new PageResult<List<?>>();
		pageResult.setStatus(HttpStatus.OK);
		pageResult.setMessage("查詢成功!");
		pageResult.setData(this.queryData.getResultList());
		pageResult.setCurrent(this.pagable.getPageNumber());
		pageResult.setTotal(getTotal());
		pageResult.setPageSize(this.pagable.getPageSize());
		return pageResult;
	}

	/**
	 * 取得查詢筆數
	 */
	public int getTotal() {
		this.initQueryCount();
		int total = this.queryCount.getResultList().size();
		log.info("total = " + total);
		return total;
	}

	/**
	 * 執行 Update 或 Delete 指令
	 * 
	 * @return 影響筆數
	 */
	public int executeUpdate() {
		this.init();
		return this.queryData.executeUpdate();
	}

	/**
	 * 插入 SQL
	 */
	public void add(Object... ob) {
		if (ob.length == 1) {
			this.sql.append(" " + String.valueOf(ob[0]) + " ");
		} else {
			this.combine(ob);
		}
	}

	/**
	 * 初始化 Count 用的Query物件
	 */
	private void initQueryCount() {
		this.queryCount = this.entityManager.createNativeQuery(this.sql.toString());
		this.genParameter(queryCount);
	}

	/**
	 * 初始化查詢用的Query物件
	 */
	private void initQueryData() {
		if (clazz == null) {
			this.queryData = this.entityManager.createNativeQuery(this.sql.toString());
			this.queryData.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		} else {
			this.queryData = entityManager.createNativeQuery(sql.toString(), clazz);
		}
		this.genParameter(queryData);
	}

	/**
	 * 初始化 Update 或 Delete 用的Query物件
	 */
	private void init() {
		this.queryData = this.entityManager.createNativeQuery(this.sql.toString());
		this.genParameter(queryData);
	}

	private void genParameter(Query query) {
		for (String key : this.parameters.keySet()) {
			query.setParameter(key, parameters.get(key));
		}
	}

	private void combine(Object[] ob) {
		boolean hasValue = true;
		String condition = null;

		for (int i = 0; i < ob.length; i++) {
			if (i == 0) {
				condition = String.valueOf(ob[0]);
			} else {
				hasValue = this.validIsNotNull(ob[i]);
				if (!hasValue) {
					break;
				}
			}
		}

		if (hasValue) {
			this.sql.append(" " + condition + " ");
			this.match(ob);
		}

	}

	private void match(Object[] ob) {

		String[] cursor = String.valueOf(ob[0]).split("\\s+");
		List<String> variable = new ArrayList<String>();
		for (int i = 0; i < cursor.length; i++) {
			if (cursor[i].startsWith(":")) {
				variable.add(cursor[i].substring(1, cursor[i].length()));
			}
		}

		if (variable.size() != ob.length - 1) {
			log.info("查詢條件與parameter不相同!!");
		} else {
			for (int i = 0; i < variable.size(); i++) {

				if (ob[i + 1] instanceof String) {
					this.parameters.put(variable.get(i), String.valueOf(ob[i + 1]));
				} else if (ob[i + 1] instanceof Integer) {
					this.parameters.put(variable.get(i), Integer.parseInt(String.valueOf(ob[i + 1])));
				} else if (ob[i + 1] instanceof Long) {
					this.parameters.put(variable.get(i), Long.parseLong(String.valueOf(ob[i + 1])));
				} else if (ob[i + 1] instanceof Double) {
					this.parameters.put(variable.get(i), Double.parseDouble(String.valueOf(ob[i + 1])));
				} else if (ob[i + 1] instanceof Float) {
					this.parameters.put(variable.get(i), Float.parseFloat(String.valueOf(ob[i + 1])));
				} else if (ob[i + 1] instanceof Boolean) {
					this.parameters.put(variable.get(i), Boolean.parseBoolean(String.valueOf(ob[i + 1])));
				} else {
					this.parameters.put(variable.get(i), ob[i + 1]);
				}

			}
		}

	}

	private boolean validIsNotNull(Object ob) {
		if (null == ob || StringUtils.isBlank(String.valueOf(ob))) {
			return false;
		}
		return true;
	}

}
