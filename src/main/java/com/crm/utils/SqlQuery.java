package com.crm.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 提供組合SQL需要的方法
 */
@Component
@Slf4j
public class SqlQuery {
	private StringBuilder sqlBuilder; // SQL字串
	private Map<String, Object> parameters; // SQL參數
	private final String SPACE = " "; // 空白
	private final String PARAM_TAG = ":"; // SQL參數符號

	public SqlQuery() {
		sqlBuilder = new StringBuilder();
		parameters = new HashMap<String, Object>();
	}

	/**
	 * 插入 SQL
	 * @param sql SQL
	 * @param parameters SQL 參數
	 */
	public void append(String sql, Object... parameters) {
		if (sql.startsWith(SPACE)) {
			sqlBuilder.append(sql);
		} else {
			sqlBuilder.append(SPACE + sql);
		}

		setParameters(sql, parameters);
	}
	
	/**
	 * 參數不為 null 或  empty 或空白字串時插入 SQL
	 * @param sql SQL
	 * @param parameters SQL 參數
	 */
	public void appendNotBlank(String sql, String parameters) {
		
		if (StringUtils.isNotBlank(parameters)) {
			if (sql.startsWith(SPACE)) {
				sqlBuilder.append(sql);
			} else {
				sqlBuilder.append(SPACE + sql);
			}

			setParameters(sql, new Object[] {parameters});	
		}
	}

	/**
	 * 取得執行SQL
	 */
	public String getSql() {
		return sqlBuilder.toString();
	}

	/**
	 * 取得參數
	 */
	public Map<String, Object> getParameters() {
		return parameters;
	}

	/**
	 * 取得組合參數後的SQL
	 */
	public String getPlainSql() {
		String sql = sqlBuilder.toString();

		for (Entry<String, Object> param : parameters.entrySet()) {
			sql = sql.replace(PARAM_TAG + param.getKey(), String.format("'%s'", param.getValue()));
		}

		return sql;
	}

	/**
	 * 寫SQL
	 */
	public void WriteLog() {
		log.info(getPlainSql());
	}

	private void setParameters(String sql, Object[] params) {

		if (params.length == 0)
			return;

		String[] sqlSlices = String.valueOf(sql).split("\\s+");
		List<String> paramNames = new ArrayList<String>();

		for (String slice : sqlSlices) {
			if (slice.startsWith(PARAM_TAG)) {
				paramNames.add(slice.substring(1, slice.length()));
			}
		}

		for (int i = 0; i < paramNames.size(); i++) {
			String paramName = paramNames.get(i);
			String paramValue = String.valueOf(params[i]);
			Object orgParam = params[i];

			if (orgParam instanceof String) {
				this.parameters.put(paramName, paramValue);
			} else if (orgParam instanceof Integer) {
				this.parameters.put(paramName, Integer.parseInt(paramValue));
			} else if (orgParam instanceof Long) {
				this.parameters.put(paramName, Long.parseLong(paramValue));
			} else if (orgParam instanceof Double) {
				this.parameters.put(paramName, Double.parseDouble(paramValue));
			} else if (orgParam instanceof Float) {
				this.parameters.put(paramName, Float.parseFloat(paramValue));
			} else if (orgParam instanceof Boolean) {
				this.parameters.put(paramName, Boolean.parseBoolean(paramValue));
			} else if (orgParam instanceof Iterator) {
				@SuppressWarnings("rawtypes")
				Iterator iterator = (Iterator) orgParam;
				List<String> list = new ArrayList<String>();

				while (iterator.hasNext()) {
					list.add(String.valueOf(iterator.next()));
				}

				this.parameters.put(paramName, String.format("'%s'", StringUtils.join(list, "', '")));
			} else {
				this.parameters.put(paramName, orgParam);
			}
		}
	}
}
