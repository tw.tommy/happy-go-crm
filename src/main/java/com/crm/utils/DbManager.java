package com.crm.utils;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.crm.common.PageResult;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Component
@Slf4j
public class DbManager {

	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * 建立 {@link SqlQuery} 物件
	 * 
	 * @return {@link SqlQuery} 物件
	 */
	public static SqlQuery createSqlQuery() {
		return new SqlQuery();
	}
	
	/**
	 * 查詢單筆資料
	 * @param <T> 泛型參數
	 * @param sqlQuery  {@link SqlQuery} 物件
	 * @param type Class 泛型參數
	 * @return 返回單筆資料
	 */
	@SuppressWarnings("unchecked")
	public <T> T findOne(SqlQuery sqlQuery, Class<T> type) {
		sqlQuery.WriteLog();
		Query query = entityManager.createNativeQuery(sqlQuery.getSql(), type);
		genParameter(query, sqlQuery.getParameters());
		return (T) query.getSingleResult();
	}
	
	/**
	 * 查詢資料
	 * @param <T> 泛型參數
	 * @param sqlQuery  {@link SqlQuery} 物件
	 * @param type Class 泛型參數
	 * @return 返回資料
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> find(SqlQuery sqlQuery, Class<T> type) {
		sqlQuery.WriteLog();
		Query query = entityManager.createNativeQuery(sqlQuery.getSql(), type);
		genParameter(query, sqlQuery.getParameters());
		return query.getResultList();
	}

	/**
	 * 查詢分頁資料
	 * @param <T> 泛型參數
	 * @param sqlQuery  {@link SqlQuery} 物件
	 * @param type Class 泛型參數
	 * @param current 查詢頁
	 * @param pageSize 分頁大小
	 * @return 返回分頁資料
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> find(SqlQuery sqlQuery, Class<T> type, int current, int pageSize) {
		sqlQuery.WriteLog();
		Query query = entityManager.createNativeQuery(sqlQuery.getSql(), type);
		genParameter(query, sqlQuery.getParameters());
		query.setFirstResult(current - 1);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	/**
	 * 查詢分頁資料
	 * @param <T> 泛型參數
	 * @param sqlQuery  {@link SqlQuery} 物件
	 * @param type Class 泛型參數
	 * @param current 查詢頁
	 * @param pageSize 分頁大小
	 * @return 以 {@link PageResult} 返回分頁資料
	 */
	public <T> List<T> findPage(SqlQuery sqlQuery, Class<T> type, int current, int pageSize) {
		if (pageSize == 0) {
			pageSize = 10;
		}
		
		if (pageSize > 1000) {
			pageSize = 1000;	//限制每頁最大筆數(防弱掃)
		}
		
		List<T> list = find(sqlQuery, type, current, pageSize);
		
		PageResult<T> pr = new PageResult<T>();
		pr.setContent(list);
		pr.setTotal(count(sqlQuery));
		pr.setCurrent(current - 1);
		pr.setPageSize(pageSize);
		return null;
	}
	
	/**
	 * 取得查詢筆數
	 * @param sqlQuery {@link SqlQuery} 物件
	 * @return 總筆數
	 */
	public int count(SqlQuery sqlQuery) {
		String sql = String.format("SELECT COUNT(*) FROM (%s) AS T", sqlQuery.getSql());
		log.info(sql);
		Query query = entityManager.createNativeQuery(sql);
		genParameter(query, sqlQuery.getParameters());
		return Integer.parseInt(query.getSingleResult().toString());
	}
	
	/**
	 * 執行 Update 或 Delete SQL
	 * @param <T> 泛型參數
	 * @param sqlQuery  {@link SqlQuery} 物件
	 * @return 返回分頁資料
	 */
	public int executeQuery(SqlQuery sqlQuery) {
		sqlQuery.WriteLog();
		Query query = entityManager.createNativeQuery(sqlQuery.getSql());
		genParameter(query, sqlQuery.getParameters());
		return query.executeUpdate();
	}

	private void genParameter(Query query, Map<String, Object> parameters) {
		for (String key : parameters.keySet()) {
			query.setParameter(key, parameters.get(key));
		}
	}
}
