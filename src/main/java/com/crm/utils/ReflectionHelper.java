package com.crm.utils;

import java.lang.reflect.Method;

import org.springframework.stereotype.Component;

/**
 * 一堆Reflection方法
 *
 */
@Component
public class ReflectionHelper {

	/**
	 * 取得欄位值
	 * @param instance 要取得值的instance
	 * @param fieldName 欄位名
	 * @return
	 */
	public Object getFieldValue(Object instance, String fieldName) {

		try {
			// 呼叫 getXXXX 方法
			String methodName = getMethodNameforGet(fieldName);
			Method method = instance.getClass().getDeclaredMethod(methodName);
			return method.invoke(instance);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public void setFieldValue(Object instance, String fieldName, String value) {

		try {
			// 呼叫 setXXXX 方法
			String methodName = getMethodNameforSet(fieldName);
			//System.out.println("fieldName = " + fieldName + " ; methodName = " + methodName);
			Method method = instance.getClass().getDeclaredMethod(methodName, new Class[] { value.getClass() });
			method.invoke(instance, new Object[] { value });

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 輸入fieldName回傳get方法名
	 * @param fieldName ex: groupId
	 * @return ex: getGroupId
	 */
	public String getMethodNameforGet(String fieldName) {
		return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	/**
	 * 輸入fieldName回傳set方法名
	 * @param fieldName ex: groupId
	 * @return ex: setGroupId
	 */
	public String getMethodNameforSet(String fieldName) {
		return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	/**
	 * 輸入fieldName回傳[prefix]方法名
	 * @param prefix ex: conv
	 * @param fieldName ex: groupId
	 * @return ex: convGroupId
	 */
	public String getMethodName(String fieldName) {
		if (!fieldName.startsWith("conv")) {
			return "conv" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);	
		}
		else {
			return fieldName;
		}
	}
}
