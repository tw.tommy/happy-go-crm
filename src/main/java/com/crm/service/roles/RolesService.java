package com.crm.service.roles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.repository.RolesRepository;

@Service
public class RolesService {

	@Autowired
	@Qualifier("rolesRepositoryS")
	private RolesRepository rolesRepository;
	
	@Transactional(readOnly = true)
	public List<String> findRoleById(String username){
		return rolesRepository.findRoleByUsername(username);
	}
	
	
}
