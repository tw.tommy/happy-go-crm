package com.crm.service.loginAttempt;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.LoginAttempt;
import com.crm.repository.LoginAttemptRepository;

@Service
public class LoginAttemptService {

	@Autowired
	private LoginAttemptRepository loginAttemptRepository;

	@Transactional(noRollbackFor = Exception.class)
	public void save(String username, int attempt) {
		LoginAttempt loginAttempt = new LoginAttempt();
		loginAttempt.setUsername(username);
		loginAttempt.setAttempt(attempt);
		if(attempt==3) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");
			Calendar date = Calendar.getInstance();
			long t= date.getTimeInMillis();
			Date add15min=new Date(t + (15 * 60000));
			loginAttempt.setAllowDate(sdf.format(add15min));
		}
		
		loginAttemptRepository.save(loginAttempt);
	}
	
	@Transactional(readOnly = true)
	public LoginAttempt findByOne(String username) {
		return loginAttemptRepository.findByOne(username); 
	}
	
	@Transactional(noRollbackFor = Exception.class)
	public void delete(String username) {
		loginAttemptRepository.delete(username);
	}
}
