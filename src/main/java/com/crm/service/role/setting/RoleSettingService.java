package com.crm.service.role.setting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.common.oTo;
import com.crm.model.entity.RoleSetting;
import com.crm.repository.RoleSettingRepository;
import com.crm.restController.admin.role.InsertForm;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RoleSettingService {

	@Autowired
	@Qualifier("rolesRepository")
	private RoleSettingRepository roleSettingRepository;

	@Transactional(readOnly = true)
	public List<RoleSetting> findAll() {
		return roleSettingRepository.findByEnabledTrue();
	}

	@Transactional(rollbackFor = Exception.class)
	public void save(InsertForm form) throws Exception {
		try {
			RoleSetting tbSettingRole=(RoleSetting) oTo.fill(form, RoleSetting.class);
			roleSettingRepository.save(tbSettingRole);
		} catch (Exception e) {
			throw new Exception("新增失敗!",e);
		}
	}

	@Transactional(readOnly = true)
	public List<RoleSetting> findAllByEnabled(String enabled) {
		if ("ALL".equals(enabled)) {
			return roleSettingRepository.findAll();
		} else if ("true".equals(enabled)) {
			return roleSettingRepository.findByEnabledTrue();
		} else if ("false".equals(enabled)) {
			return roleSettingRepository.findByEnabledFalse();
		}
		return null;
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(String role) throws Exception {
		try {
			roleSettingRepository.deleteById(role);
		} catch (Exception ex) {
			throw new Exception("刪除失敗!",ex);
		}
	}
	

	@Transactional(rollbackFor = Exception.class)
	public void upate(InsertForm form) throws Exception {
		try {
			RoleSetting roleSetting = new RoleSetting();
			roleSetting.setRole(form.getRole());
			roleSetting.setName(form.getName());
			roleSetting.setDef(form.getDef());
			roleSetting.setEnabled(form.isEnabled());
			roleSettingRepository.save(roleSetting);
		} catch (Exception ex) {
			throw new Exception("刪除失敗!",ex);
		}
	}

}
