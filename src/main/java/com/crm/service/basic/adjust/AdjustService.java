package com.crm.service.basic.adjust;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.common.PageResult;
import com.crm.model.converter.AdjustConverter;
import com.crm.model.entity.TbCust;
import com.crm.model.view.adjust.ListVo;
import com.crm.model.view.adjust.QueryForm;
import com.crm.model.view.adjust.UpdateForm;
import com.crm.repository.CustRepository;
import com.crm.service.ValueConverterService;
import com.crm.utils.DbManager;
import com.crm.utils.SqlQuery;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AdjustService {

	@Autowired
	private CustRepository custRespository;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DbManager manager;

	@Autowired
	private AdjustConverter converter;
	
	@Autowired
	private ValueConverterService converterService;

	// 查詢列表頁資料
	public PageResult<ListVo> getListData(QueryForm form, int current, int pageSize) {
		List<ListVo> list = new ArrayList<ListVo>();
		SqlQuery query = new SqlQuery();
		query.append("SELECT * FROM TB_CUST where 1=1");
		query.appendNotBlank("and CUST_ID = :CustId ", form.getCustId());
		List<TbCust> items = manager.find(query, TbCust.class, current, pageSize);
		int total = manager.count(query);

		for (TbCust item : items) {
			ListVo dto = converter.convert(item);
			list.add(dto);
		}
		
		list = converterService.converts(list);
		PageResult<ListVo> pr = new PageResult<ListVo>();
		pr.setData(list);
		pr.setTotal(total);
		pr.setCurrent(current);
		pr.setPageSize(pageSize);
		return pr;
	}

	// 新增
	@Transactional(rollbackFor = Exception.class)
	public TbCust add(TbCust cust) {
		return custRespository.save(cust);
	}

	// 修改
	@Transactional(rollbackFor = Exception.class)
	public TbCust update(UpdateForm form) {
		TbCust cust = custRespository.findOne(form.getRegionId(), form.getCustId());
		cust.setCustLevel(form.getCustLevel());
		cust.setEngName(form.getEngName());
		cust.setLocName(form.getLocName());
		cust.setPersonType(form.getPersonType());
		cust.setPersonId(form.getPersonId());
		return custRespository.save(cust);
	}

	// 刪除
	@Transactional(rollbackFor = Exception.class)
	public void delete(UpdateForm form) {
		TbCust cust = custRespository.findOne(form.getRegionId(), form.getCustId());
		custRespository.delete(cust);
	}

	public int countByPersonId(String personId) {
		return custRespository.countByPersonId(personId);
	}
}