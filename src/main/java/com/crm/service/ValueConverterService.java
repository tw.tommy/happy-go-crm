package com.crm.service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.crm.annotation.ValueConverter;
import com.crm.utils.ReflectionHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ValueConverterService {

	@Autowired
	public ReflectionHelper reflectionHelper;

	/**
	 * 做 Value Converter
	 * 
	 * @param converterName 要呼叫的Converter ex: convGroupStatus(String value) 的
	 *                      Converter Name 是 GroupStatus
	 * @param value         要Convert的值
	 * @return
	 */
	public String convert(String converterName, String value) {
		String retVal = "";

		if (StringUtils.isEmpty(converterName))
			return value;

		try {
			// 由converterName決定要呼叫那個Method
			String methodName = reflectionHelper.getMethodName(converterName);

			// 呼叫方法
			Method method = this.getClass().getDeclaredMethod(methodName, new Class[] { String.class });
			retVal = (String) method.invoke(this, new Object[] { value });
		} catch (Exception e) {
			log.error("converterName = " + converterName + ", value = " + value + ", error message=" + e.getMessage());
		}

		// 若轉換出來是null or empty, 則回傳原值
		if (StringUtils.isEmpty(retVal))
			return value;
		else
			return retVal;
	}

	/**
	 * 做 Value Converter
	 * 
	 * @param entity 要進行Convert的Entity
	 */
	public <T> T convert(T entity) {

		if (entity == null)
			return null;

		Field[] fields = entity.getClass().getDeclaredFields(); // 取得所有 Field

		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];

			// 取出Annotation是ValueConverter的Field
			Annotation annotation = field.getDeclaredAnnotation(ValueConverter.class);

			if (annotation != null) {
				ValueConverter valueConverterAnnotation = ((ValueConverter) annotation);

				// 取得要進行Convert的值
				String fieldName = valueConverterAnnotation.field();

				Object wantToConvertValue = reflectionHelper.getFieldValue(entity, fieldName);

				if (wantToConvertValue == null)
					continue;

				// 呼叫Converter取得結果
				String converterName = valueConverterAnnotation.name();

				if (StringUtils.isEmpty(converterName))
					converterName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

				String resultValue = this.convert(converterName, wantToConvertValue.toString());

				// 儲存轉完的結果
				reflectionHelper.setFieldValue(entity, field.getName(), resultValue);
			}
		}

		return entity;
	}

	/**
	 * 做 Value Converter
	 * 
	 * @param <T>
	 * @param entitys 要進行Convert的Entitys
	 */
	public <T> Page<T> converts(Page<T> entitys) {

		if (entitys == null)
			return null;

		for (T item : entitys.getContent()) {
			this.convert(item);
		}

		return entitys;
	}

	/**
	 * 做 Value Converter
	 * 
	 * @param <T>
	 * @param entitys 要進行Convert的Entitys
	 */
	public <T> List<T> converts(List<T> entitys) {

		if (entitys == null)
			return null;

		for (T item : entitys) {
			this.convert(item);
		}

		return entitys;
	}

	/**
	 * 預設的日期 Converter
	 */
	public String convDate(String value) {
		if (StringUtils.isNotEmpty(value)) {

			if (value.length() == 8) {
				return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6, 8);
			}

			if (value.length() == 6) {
				return value.substring(0, 4) + "-" + value.substring(4, 6);
			}
		}

		return value;
	}

	/**
	 * 預設的時間 Converter
	 */
	public String convTime(String value) {
		if (StringUtils.isNotEmpty(value) && value.length() == 6) {
			return value.substring(0, 2) + ":" + value.substring(2, 4) + ":" + value.substring(4, 6);

		}
		return value;
	}

	/**
	 * 數值的 Converter
	 */
	public String convNumber(String value) {
		if (StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value)) {

			NumberFormat nf = NumberFormat.getInstance();
			return nf.format(Long.parseLong(value));
		}
		return value;
	}

	/**
	 * 數值的 Converter
	 * 
	 * @param value
	 * @param minFractionDigits 至小數點幾位
	 * @return
	 */
	public String convNumber(String value, int minFractionDigits) {
		if (StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value)) {

			NumberFormat nf = NumberFormat.getInstance();
			nf.setMinimumFractionDigits(minFractionDigits);
			return nf.format(Long.parseLong(value));
		}
		return value;
	}

	/**
	 * 金額的 Converter
	 */
	public String convMoney(String value) {
		if (StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value)) {
			// 表示至少要有個位數，且每三位數以一個逗號分開
			DecimalFormat df = new DecimalFormat("$#,##0.##");
			return df.format(Long.parseLong(value));
		}
		return value;
	}

	/**
	 * 轉成百分比的 Converter
	 */
	public String convPercentage(BigDecimal value) {
		DecimalFormat df = new DecimalFormat("#,##0.##%");
		return df.format(value);
	}

	/**
	 * TB_CUST.PERSON_TYPE
	 */
	public String convPersonType(String value) {
		if ("1".contentEquals(value)) {
			return "本國人";
		}
		else if ("2".contentEquals(value)) {
			return "外藉人士";
		}
		else {
			return value;
		}
	}
}
