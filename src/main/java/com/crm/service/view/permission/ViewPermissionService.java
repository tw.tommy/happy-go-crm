package com.crm.service.view.permission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.ViewPermission;
import com.crm.repository.ViewPermissionRepository;

@Service
public class ViewPermissionService{

	@Autowired
	private ViewPermissionRepository viewPermissionRepository;
	
	@Transactional(readOnly = true)
	public List<ViewPermission> findAll() {
		return viewPermissionRepository.findAll();
	}

}
