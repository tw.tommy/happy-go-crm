package com.crm.service.path.view.permission.rel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.PathViewPermissionRel;
import com.crm.model.entity.PathViewPermissionRelPk;
import com.crm.repository.PathViewPermissionRelRepositroy;
import com.crm.restController.admin.func.PathFuncAddForm;

@Service
public class PathViewPermissionRelService {

	@Autowired
	private PathViewPermissionRelRepositroy pathViewPermissionRelRepositroy;
	
	@Transactional(rollbackFor = Exception.class)
	public void save(PathFuncAddForm form) throws Exception {
		try {
			List<PathViewPermissionRel> list = new ArrayList<>();
			for(String index : form.getViewPermission()) {
				PathViewPermissionRel ob = new PathViewPermissionRel();
				PathViewPermissionRelPk id = new PathViewPermissionRelPk();
				id.setPath(form.getPath());
				id.setViewPermission(index);
				ob.setId(id);
				list.add(ob);
			}
			pathViewPermissionRelRepositroy.delete(form.getPath());
			pathViewPermissionRelRepositroy.saveAll(list);
		}catch(Exception e) {
			throw new Exception("新增失敗!",e);
		}
		
	}
	
	public List<String> findviewPermissionByPath(String path){
		return pathViewPermissionRelRepositroy.findviewPermissionByPath(path);
	}

}
