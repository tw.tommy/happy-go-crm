package com.crm.service.path.view.permission.role.rel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.ApiPermissionSetting;
import com.crm.model.entity.ApiPermissionSettingPk;
import com.crm.model.entity.PathViewPermissionRoleRel;
import com.crm.model.entity.PathViewPermissionRoleRelPk;
import com.crm.repository.ApiPermissionSettingRepository;
import com.crm.repository.PathViewPermissionRoleRelRepository;
import com.crm.restController.admin.permission.SettingForm;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PathViewPermissionRoleRelService {

	@Autowired
	private PathViewPermissionRoleRelRepository pathViewPermissionRoleRelRepository;

	@Autowired
	private ApiPermissionSettingRepository apiPermissionSettingRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public void save(SettingForm form) throws Exception {
		try {
			String role = form.getRole();
		
			//刪除 PATH_VIEW_PERMISSION_ROLE_REL
			PathViewPermissionRoleRel delMain = new PathViewPermissionRoleRel();
			PathViewPermissionRoleRelPk delMainId = new PathViewPermissionRoleRelPk();
			delMainId.setRole(role);
			delMainId.setPath(form.getTitle());
			delMainId.setViewPermission("NONE");
			delMain.setId(delMainId);
			pathViewPermissionRoleRelRepository.delete(delMain);

			for (String index : form.getExcludePaths()) {
				PathViewPermissionRoleRel tbRolesPermission = new PathViewPermissionRoleRel();
				PathViewPermissionRoleRelPk id = new PathViewPermissionRoleRelPk();
				id.setRole(role);
				String[] temp = index.split("&");
				if (temp.length > 1) {
					id.setPath(temp[0]);
					id.setViewPermission(temp[1]);
					ApiPermissionSettingPk pk = new ApiPermissionSettingPk(); 
					pk.setPath(temp[0]+"/"+temp[1]);
					pk.setRole(role);
					ApiPermissionSetting apiPermissionSetting = new ApiPermissionSetting();
					apiPermissionSetting.setId(pk);
					apiPermissionSettingRepository.delete(apiPermissionSetting);
				} else {
					id.setPath(temp[0]);
					id.setViewPermission("NONE");
				}
				tbRolesPermission.setId(id);
				pathViewPermissionRoleRelRepository.delete(tbRolesPermission);
			}
			
			//新增Tb_SETTING_PATH_PERMISSION
			List<PathViewPermissionRoleRel> list = new ArrayList<>();
			
			List<ApiPermissionSetting> apiList = new ArrayList<>();
			
			if (!form.getPaths().isEmpty()) {
				PathViewPermissionRoleRel main = new PathViewPermissionRoleRel();
				PathViewPermissionRoleRelPk mainId = new PathViewPermissionRoleRelPk();
				mainId.setRole(role);
				mainId.setPath(form.getTitle());
				mainId.setViewPermission("NONE");
				main.setId(mainId);
				list.add(main);
			}
			for (String index : form.getPaths()) {
				PathViewPermissionRoleRel pathViewPermissionRoleRel = new PathViewPermissionRoleRel();
				PathViewPermissionRoleRelPk id = new PathViewPermissionRoleRelPk();
				id.setRole(role);
				String[] temp = index.split("&");
				if (temp.length > 1) {
					id.setPath(temp[0]);
					id.setViewPermission(temp[1]);
					//api TbSettingApiPermission, TbSettingApiPermissionPk
					ApiPermissionSettingPk pk = new ApiPermissionSettingPk(); 
					pk.setPath(temp[0]+"/"+temp[1]);
					pk.setRole(role);
					ApiPermissionSetting apiPermissionSetting = new ApiPermissionSetting();
					apiPermissionSetting.setId(pk);
					apiList.add(apiPermissionSetting);
				} else {
					id.setPath(temp[0]);
					id.setViewPermission("NONE");
				}
				pathViewPermissionRoleRel.setId(id);
				list.add(pathViewPermissionRoleRel);
			}
			pathViewPermissionRoleRelRepository.saveAll(list);
			apiPermissionSettingRepository.saveAll(apiList);
		} catch (Exception e) {
			throw new Exception("新增失敗!",e);
		}

	}

	public List<String> getIdByRole(String role) {
		return pathViewPermissionRoleRelRepository.getIdByRole(role);
	}

	public List<Map<String, Object>> getAllLegalFunc() {
		log.info("UserProfileUtils.getRoles() " + UserProfileUtils.getRoles());
		return pathViewPermissionRoleRelRepository.getAllLegalFunc(UserProfileUtils.getRoles());
	}

}
