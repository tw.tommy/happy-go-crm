package com.crm.service.path.setting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.PathSetting;
import com.crm.repository.PathViewPermissionRelRepositroy;
import com.crm.repository.PathSettingRepository;
import com.crm.restController.admin.path.SettingUpdateForm;
import com.crm.restController.admin.path.SettingUpdateRedirectForm;
import com.crm.restController.api.PathForm;
import com.crm.restController.api.PathListForm;
import com.crm.service.path.setting.PathSettingService;
import com.crm.utils.DataManager;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PathSettingService {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private PathSettingRepository pathSettingRepository;

	@Autowired
	private PathViewPermissionRelRepositroy pathViewPermissionRelRepositroy;

	@Transactional(rollbackFor = Exception.class)
	public void save(List<PathForm> pathForms) throws Exception {
		try {
			List<PathSetting> paths = new ArrayList<PathSetting>();
			for (int i = 0; i < pathForms.size(); i++) {
				PathForm pathForm = pathForms.get(i);
				PathSetting tbPath = new PathSetting();
				if (i == 0) {
					tbPath.setId("/" + pathForm.getPath());
					tbPath.setName(pathForm.getPath());
					tbPath.setRedirect("noRedirect");
					tbPath.setPath(pathForm.getPath());
					tbPath.setTitle(pathForm.getTitle());
					tbPath.setIcon(pathForm.getIcon());
					tbPath.setComponent("Layout");
					tbPath.setTier(i);
					tbPath.setHidden(false);
				} else if (i == 1) {
					String redirect = "/" + pathForms.get(0).getPath();
					redirect = redirect + "/" + pathForms.get(1).getPath();
					redirect = redirect + "/" + pathForms.get(2).getPath();
					redirect = redirect + "/" + pathForms.get(3).getPath();
					String name = "/" + pathForms.get(0).getPath();
					name = name + "/" + pathForms.get(1).getPath();
					tbPath.setId(name);
					tbPath.setName(name);
					tbPath.setRedirect(redirect);
					tbPath.setPath(pathForm.getPath());
					tbPath.setTitle(pathForm.getTitle());
					tbPath.setIcon(pathForm.getIcon());
					tbPath.setComponent("Menu");
					tbPath.setTier(i);
					tbPath.setHidden(false);
					tbPath.setParent("/" + pathForms.get(0).getPath());
				} else if (i == 2) {
					String redirect = "/" + pathForms.get(0).getPath();
					redirect = redirect + "/" + pathForms.get(1).getPath();
					redirect = redirect + "/" + pathForms.get(2).getPath();
					redirect = redirect + "/" + pathForms.get(3).getPath();
					String name = "/" + pathForms.get(0).getPath();
					name = name + "/" + pathForms.get(1).getPath();
					name = name + "/" + pathForms.get(2).getPath();
					String parent = "/" + pathForms.get(0).getPath();
					parent = parent + "/" + pathForms.get(1).getPath();
					tbPath.setId(name);
					tbPath.setName(name);
					tbPath.setRedirect(redirect);
					tbPath.setPath(pathForm.getPath());
					tbPath.setTitle(pathForm.getTitle());
					tbPath.setIcon(pathForm.getIcon());
					tbPath.setComponent("SubMenu");
					tbPath.setTier(i);
					tbPath.setHidden(true);
					tbPath.setParent(parent);
				} else if (i == 3) {
					String component = pathForms.get(0).getPath();
					component = component + "_" + pathForms.get(1).getPath();
					component = component + "_" + pathForms.get(2).getPath();
					component = component + "_" + pathForms.get(3).getPath();
					String name = "/" + pathForms.get(0).getPath();
					name = name + "/" + pathForms.get(1).getPath();
					name = name + "/" + pathForms.get(2).getPath();
					name = name + "/" + pathForms.get(3).getPath();
					String parent = "/" + pathForms.get(0).getPath();
					parent = parent + "/" + pathForms.get(1).getPath();
					parent = parent + "/" + pathForms.get(2).getPath();
					tbPath.setId(name);
					tbPath.setPath(pathForm.getPath());
					tbPath.setName(name);
					tbPath.setTitle(pathForm.getTitle());
					tbPath.setIcon(pathForm.getIcon());
					tbPath.setComponent(component);
					tbPath.setTier(i);
					tbPath.setHidden(true);
					tbPath.setParent(parent);
				}
				paths.add(tbPath);
			}
			pathSettingRepository.saveAll(paths);
		} catch (Exception ex) {
			throw new Exception("新增失敗!",ex);
		}
	}

	//@Cacheable(cacheNames = "url" , key = "#role")
	@Transactional(readOnly = true)
	public String getURL(List<String> role) {
		JSONArray allPath = new JSONArray();
		List<PathSetting> oneTier = pathSettingRepository.findAllOneTierByRole(role);
		if (null != oneTier) {
			for (PathSetting index : oneTier) {
				JSONObject parent = new JSONObject();
				JSONObject meta = new JSONObject();
				parent.put("path", "/" + index.getPath());
				parent.put("name", index.getName());
				parent.put("redirect", index.getRedirect());
				meta.put("title", index.getTitle());
				meta.put("icon", index.getIcon());
				parent.put("meta", meta);
				parent.put("hidden", index.isHidden());
				parent.put("component", index.getComponent());
				this.getURL(index.getTier(), index.getId(), parent);
				allPath.put(parent);
			}
		}
		return allPath.toString();
	}

	@Transactional(readOnly = true)
	public void getURL(int tier, String id, JSONObject json) {
		List<PathSetting> childrenPath = pathSettingRepository.findAllByParentAndRole(UserProfileUtils.getRoles(), id,
				tier + 1);
		JSONArray allPath = new JSONArray();

		if (childrenPath.size() > 0) {
			for (PathSetting index : childrenPath) {
				JSONObject parent = new JSONObject();
				JSONObject meta = new JSONObject();
				parent.put("path", index.getPath());
				parent.put("name", index.getName());
				meta.put("title", index.getTitle());
				meta.put("icon", index.getIcon());
				parent.put("meta", meta);
				parent.put("hidden", index.isHidden());
				parent.put("component", index.getComponent());
				if (index.getRedirect() != null) {
					parent.put("redirect", index.getRedirect());
				}
				this.getURL(index.getTier(), index.getId(), parent);
				allPath.put(parent);
			}
			json.put("children", allPath);
		}

	}

	@Transactional(readOnly = true)
	public JSONArray getAllPath() {
		JSONArray allPath = new JSONArray();
		List<PathSetting> oneTier = pathSettingRepository.findAllOneTier();
		if (null != oneTier) {
			for (PathSetting index : oneTier) {
				JSONObject parent = new JSONObject();
				parent.put("id", index.getId());
				parent.put("path", index.getPath());
				parent.put("title", index.getTitle());
				parent.put("icon", index.getIcon());
				allPath.put(parent);
				this.getAllPath(index.getTier(), index.getId(), index.getPath(), index.getTitle(), allPath);
			}
		}
		return allPath;
	}

	@Transactional(readOnly = true)
	public void getAllPath(int tier, String parentStr, String path, String title, JSONArray allPath) {
		List<PathSetting> childrenPath = pathSettingRepository.findAllByParentAndTier(parentStr, tier + 1);
		if (!childrenPath.isEmpty()) {
			for (PathSetting index : childrenPath) {
				JSONObject parent = new JSONObject();
				parent.put("id", index.getId());
				parent.put("path", path + "/" + index.getPath());
				parent.put("title", title + "/" + index.getTitle());
				parent.put("icon", index.getIcon());
				allPath.put(parent);
				this.getAllPath(index.getTier(), index.getId(), parent.getString("path"), parent.getString("title"),
						allPath);
			}
		}
	}

	@Transactional(readOnly = true)
	public List<Map<String, Object>> getFullPathAndTitle() {
		return pathSettingRepository.getFullPathAndTitle();
	}

	@Transactional(readOnly = true)
	public List<PathSetting> getOneTier() {
		return pathSettingRepository.findAllOneTier();
	}

	@Transactional(readOnly = true)
	public JSONArray getTree(String id) {
		List<PathSetting> childrenPath = pathSettingRepository.findAllByParentAndTier(id, 1);
		JSONArray jsonArr = new JSONArray();
		for (PathSetting index : childrenPath) {
			JSONObject ob = new JSONObject();
			ob.put("id", index.getId());
			ob.put("label", index.getTitle());
			this.getTree(index.getId(), 1, ob);
			jsonArr.put(ob);
		}
		log.info("......." + jsonArr.toString());
		return jsonArr;
	}

	public void getTree(String id, int tier, JSONObject parent) {
		List<PathSetting> childrenPath = pathSettingRepository.findAllByParentAndTier(id, tier + 1);

		if (!childrenPath.isEmpty()) {
			JSONArray allPath = new JSONArray();
			for (PathSetting index : childrenPath) {
				JSONObject ob = new JSONObject();
				ob.put("id", index.getId());
				ob.put("label", index.getTitle());
				this.getTree(index.getId(), index.getTier(), ob);
				allPath.put(ob);
				parent.put("children", allPath);
			}

		} else {
			List<Map<String,Object>> list = pathViewPermissionRelRepositroy.findByPath(id);
			if (!list.isEmpty()) {
				JSONArray allPath = new JSONArray();
				for (Map<String,Object> index : list) {
					JSONObject ob = new JSONObject();
					ob.put("id", index.get("PATH") + "&" + index.get("PERMISSION"));
					ob.put("label", index.get("NAME"));
					allPath.put(ob);
				}
				parent.put("children", allPath);
			}
		}

	}

	@Transactional(readOnly = true)
	public JSONArray getAllPath(String param) {
		JSONArray allPath = new JSONArray();
		Specification<PathSetting> spec = new Specification<PathSetting>() {
			@Override
			public Predicate toPredicate(Root<PathSetting> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicatesList = new ArrayList<>();
				if (!StringUtils.isEmpty(param)) {
					Predicate id = criteriaBuilder.equal(root.get("id"), param);
					predicatesList.add(id);
				}
				Predicate tier = criteriaBuilder.equal(root.get("tier"), 0);
				predicatesList.add(tier);
				Predicate[] predicates = new Predicate[predicatesList.size()];
				return criteriaBuilder.and(predicatesList.toArray(predicates));
			}
		};

		List<PathSetting> oneTier = pathSettingRepository.findAll(spec, Sort.by("sort"));
		if (null != oneTier) {
			for (PathSetting index : oneTier) {
				JSONObject parent = new JSONObject();
				parent.put("id", index.getId());
				parent.put("path", index.getPath());
				parent.put("title", index.getTitle());
				parent.put("icon", index.getIcon());
				allPath.put(parent);
				this.getAllPath(index.getTier(), index.getId(), index.getPath(), index.getTitle(), allPath);
			}
		}
		return allPath;
	}

	@Transactional(readOnly = true)
	public List<PathSetting> getMenuByParent(String parent, int tier) {
		return pathSettingRepository.findAllByParentAndTier(parent, tier);
	}

	@Transactional(readOnly = true)
	public List<PathSetting> getPathByParent(String oneTierMenu, String twoTierMenu, String threeTierMenu) {
		if (StringUtils.isBlank(oneTierMenu) && StringUtils.isBlank(twoTierMenu)
				&& StringUtils.isBlank(threeTierMenu)) {
			return pathSettingRepository.findAllOneTier();
		} else if (!StringUtils.isBlank(threeTierMenu)) {
			Specification<PathSetting> spec = new Specification<PathSetting>() {
				@Override
				public Predicate toPredicate(Root<PathSetting> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicatesList = new ArrayList<>();
					Predicate id = criteriaBuilder.equal(root.get("parent"), threeTierMenu);
					predicatesList.add(id);
					Predicate tier = criteriaBuilder.equal(root.get("tier"), 3);
					predicatesList.add(tier);
					Predicate[] predicates = new Predicate[predicatesList.size()];
					return criteriaBuilder.and(predicatesList.toArray(predicates));
				}
			};
			return pathSettingRepository.findAll(spec, Sort.by("sort"));
		} else if (!StringUtils.isBlank(twoTierMenu)) {
			Specification<PathSetting> spec = new Specification<PathSetting>() {
				@Override
				public Predicate toPredicate(Root<PathSetting> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicatesList = new ArrayList<>();
					Predicate id = criteriaBuilder.equal(root.get("parent"), twoTierMenu);
					predicatesList.add(id);
					Predicate tier = criteriaBuilder.equal(root.get("tier"), 2);
					predicatesList.add(tier);
					Predicate[] predicates = new Predicate[predicatesList.size()];
					return criteriaBuilder.and(predicatesList.toArray(predicates));
				}
			};
			return pathSettingRepository.findAll(spec, Sort.by("sort"));
		} else if (!StringUtils.isBlank(oneTierMenu)) {
			Specification<PathSetting> spec = new Specification<PathSetting>() {
				@Override
				public Predicate toPredicate(Root<PathSetting> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicatesList = new ArrayList<>();
					Predicate id = criteriaBuilder.equal(root.get("parent"), oneTierMenu);
					predicatesList.add(id);
					Predicate tier = criteriaBuilder.equal(root.get("tier"), 1);
					predicatesList.add(tier);
					Predicate[] predicates = new Predicate[predicatesList.size()];
					return criteriaBuilder.and(predicatesList.toArray(predicates));
				}
			};
			return pathSettingRepository.findAll(spec, Sort.by("sort"));
		}
		return null;
	}

	@Transactional(rollbackFor = Exception.class)
	public void updateSort(List<String> form) throws Exception {
		try {
			for (int i = 0; i < form.size(); i++) {
				DataManager dm = new DataManager(entityManager);
				dm.add("UPDATE PATH_SETTING SET");
				dm.add("SORT = :sort", i);
				dm.add("where 1 = 1");
				dm.add("and ID = :id", form.get(i));
				dm.executeUpdate();
			}
		} catch (Exception e) {
			throw new Exception("更新排序失敗!",e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void updatePath(SettingUpdateForm updateForm) throws Exception {
		try {
			DataManager dataManager = new DataManager(entityManager);
			dataManager.add("UPDATE PATH_SETTING SET");
			dataManager.add("TITLE= :title", updateForm.getTitle());
			dataManager.add(",ICON= :icon", updateForm.getIcon());
			dataManager.add("WHERE ID = :id", updateForm.getId());
			dataManager.executeUpdate();
		} catch (Exception e) {
			throw new Exception("更新失敗!",e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void deletePath(String id) throws Exception {
		try {
			DataManager dataManagerPath = new DataManager(entityManager);
			dataManagerPath.add("DELETE PATH_SETTING");
			dataManagerPath.add("WHERE ID = :id", id);
			dataManagerPath.add("OR ID like :likeID", id+"/%");
			dataManagerPath.executeUpdate();
			
			DataManager dataManagerApiPermission = new DataManager(entityManager);
			dataManagerApiPermission.add("DELETE API_PERMISSION_SETTING");
			dataManagerApiPermission.add("WHERE PATH = :id", id);
			dataManagerApiPermission.add("OR PATH like :likeID", id+"/%");
			dataManagerApiPermission.executeUpdate();
			
			DataManager dataManagerPathPermission = new DataManager(entityManager);
			dataManagerPathPermission.add("DELETE PATH_VIEW_PERMISSION_REL");
			dataManagerPathPermission.add("WHERE PATH = :id", id);
			dataManagerPathPermission.add("OR PATH like :likeID", id+"/%");
			dataManagerPathPermission.executeUpdate();
			
			DataManager dataManagerPathRelPermission = new DataManager(entityManager);
			dataManagerPathRelPermission.add("DELETE PATH_VIEW_PERMISSION_ROLE_REL");
			dataManagerPathRelPermission.add("WHERE PATH = :id", id);
			dataManagerPathRelPermission.add("OR PATH like :likeID", id+"/%");
			dataManagerPathRelPermission.executeUpdate();
			
		} catch (Exception e) {
			throw new Exception("刪除失敗!",e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void updateRedirect(SettingUpdateRedirectForm form) throws Exception {
		try {
			pathSettingRepository.updateRedirect(form.getRedirect(),form.getId());
		} catch (Exception e) {
			throw new Exception("更新失敗!",e);
		}
	}

}
