 package com.crm.service.userProfile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.jwt.model.Roles;
import com.crm.jwt.model.RolesPk;
import com.crm.jwt.model.Users;
import com.crm.jwt.repository.RolesRepository;
import com.crm.jwt.repository.UsersRepository;
import com.crm.model.entity.UserProfile;
import com.crm.repository.UserProfileRepository;
import com.crm.restController.admin.user.UserRegisterForm;
import com.crm.service.userProfile.UserProfileService;
import com.crm.utils.DataManager;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserProfileService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Autowired
	@Qualifier("jwtRolesRepository")
	private RolesRepository rolesRepository;

	@Autowired
	private UsersRepository usersRepository;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(rollbackFor = Exception.class)
	public void save(UserRegisterForm userRegisterForm) throws Exception {
		try {
			userRegisterForm.setUsername(userRegisterForm.getUsername().toLowerCase());
		
			//log.info("userRegisterForm 1 " + userRegisterForm.getUsername());
			
			UserProfile userProfile = new UserProfile();
			userProfile.setUsername(userRegisterForm.getUsername());
			userProfile.setName(userRegisterForm.getName());
			userProfile.setImage(userRegisterForm.getImage());
			userProfile.setDepartment(userRegisterForm.getDepartment());
			userProfile.setEmail(userRegisterForm.getEmail());
			userProfile.setSex(userRegisterForm.getSex());
			userProfile.setCreateDate(DateFormatUtils.format(new Date(), "yyyyMMdd HHmm"));
			userProfile.setCreater(UserProfileUtils.getUsername());
			userProfileRepository.save(userProfile);
			
			//log.info("userRegisterForm 2 " + userRegisterForm.getUsername());
			
			Users user = new Users();
			user.setUsername(userRegisterForm.getUsername());
			user.setPassword(passwordEncoder.encode(userRegisterForm.getUsername()));
			user.setEnable(true);
			usersRepository.save(user);

			//log.info("userRegisterForm 3 " + userRegisterForm.getUsername());
			
			List<Roles> rolesList = new ArrayList<Roles>();
			
			for (String index : userRegisterForm.getRoles()) {
				RolesPk id = new RolesPk();
				id.setUsername(userRegisterForm.getUsername());
				id.setRole(index);
				Roles roles = new Roles();
				roles.setId(id);
				rolesList.add(roles);
			}
			rolesRepository.saveAll(rolesList);
			log.info("userRegisterForm 4 " + userRegisterForm.getUsername());
		} catch (Exception e) {
			throw new Exception("新增失敗!",e);
		}
	}
	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void update(UserRegisterForm userRegisterForm) throws Exception {
		try {
			userRegisterForm.setUsername(userRegisterForm.getUsername().toLowerCase());
			UserProfile userProfile = new UserProfile();
			userProfile.setUsername(userRegisterForm.getUsername());
			userProfile.setName(userRegisterForm.getName());
			userProfile.setImage(userRegisterForm.getImage());
			userProfile.setDepartment(userRegisterForm.getDepartment());
			userProfile.setEmail(userRegisterForm.getEmail());
			userProfile.setSex(userRegisterForm.getSex());
			userProfile.setCreateDate(DateFormatUtils.format(new Date(), "yyyyMMdd HHmm"));
			userProfile.setCreater(UserProfileUtils.getUsername());
			userProfileRepository.save(userProfile);
			
			
			List<Roles> rolesList = new ArrayList<Roles>();
			
			for (String index : userRegisterForm.getRoles()) {
				RolesPk id = new RolesPk();
				id.setUsername(userRegisterForm.getUsername());
				id.setRole(index);
				Roles roles = new Roles();
				roles.setId(id);
				rolesList.add(roles);
			}
			rolesRepository.deleteAll(rolesList);
			rolesRepository.saveAll(rolesList);
			
		} catch (Exception e) {
			throw new Exception("更新失敗!",e);
		}
	}

	@Transactional(readOnly = true)
	public List<Map<String, Object>> getList(Map<String, Object> params) {
		DataManager gen = new DataManager(entityManager);
		gen.add("SELECT A.*,B.ENABLE,C.ROLE FROM USER_PROFILE A LEFT JOIN USERS B ON  A.USERNAME=B.USERNAME LEFT JOIN ROLES C ON A.USERNAME=C.USERNAME WHERE 1=1 ");
		
		for(String index : params.keySet()) {
			if(!StringUtils.isBlank(String.valueOf(params.get(index)))) {
				if(index.equals("role")) {
					gen.add("AND C.ROLE = :role ",params.get(index));
				}else {
					gen.add("AND A."+index.toUpperCase()+" = :"+index+" ",params.get(index));
				}
			}
		}
		gen.add(" ORDER BY A.USERNAME");
		List<Map<String, Object>> result = (List<Map<String, Object>>) gen.getResult();
		return result;
	}

	@Transactional(readOnly = true)
	public String getImage() {
		UserProfile userProfile = userProfileRepository.getOne(UserProfileUtils.getUsername());
		return userProfile.getImage();
	}

	@Transactional(readOnly = true)
	public UserProfile findByUsername(String username) {
		Optional<UserProfile> userProfile = userProfileRepository.findById(username);
		return userProfile.orElse(new UserProfile());
	}
	
}
