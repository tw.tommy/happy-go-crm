package com.crm.service;

import org.springframework.stereotype.Component;

@Component
public abstract class BaseService {
	public static final String MSG_QUERY_FAIL = "查詢失敗";
	public static final String MSG_ADD_FAIL = "新增失敗";
	public static final String MSG_UPDATE_FAIL = "修改失敗";
	public static final String MSG_DELETE_FAIL = "刪除失敗";
}
