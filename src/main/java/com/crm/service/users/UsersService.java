package com.crm.service.users;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.common.PasswordAndReNotMatchException;
import com.crm.jwt.model.Users;
import com.crm.jwt.repository.UsersRepository;
import com.crm.restController.usersetting.Form;
import com.crm.utils.UserProfileUtils;

@Service
public class UsersService {

	@Autowired
	private PasswordEncoder passwordEncode;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public void update(Form form) throws Exception{
		Users user = usersRepository.getOne(UserProfileUtils.getUsername());
		if(!passwordEncode.matches(form.getCurrentPassword(), user.getPassword())) {
			throw new PasswordAndReNotMatchException("目前密碼與資料庫不一致!");
		}
		try {
			user.setPassword(passwordEncode.encode(form.getNewPassword()));
			usersRepository.save(user);
		}catch(Exception ex){
			throw new Exception("更新失敗!",ex);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void updateEnableByUsername(boolean enable , String username) throws Exception{
		try {
			usersRepository.updateEnabledByUsername(enable, username);
		}catch(Exception ex){
			throw new Exception("更新失敗!",ex);
		}
	}
	
	@Transactional(readOnly = true)
	public Users findUserById(String username) {
		Optional<Users> users = usersRepository.findById(username);
		return users.orElse(new Users());
	}
	
	
}
