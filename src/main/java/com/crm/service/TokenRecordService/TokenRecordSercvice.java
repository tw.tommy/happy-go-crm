package com.crm.service.TokenRecordService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.TokenRecord;
import com.crm.repository.TokenRecordRepository;

@Service
public class TokenRecordSercvice {

	@Autowired
	private TokenRecordRepository tokenRecordRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public void save(TokenRecord tokenRecord) throws Exception {
		try {
			tokenRecordRepository.save(tokenRecord);
		}catch(Exception ex) {
			throw new Exception(ex);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void update(String expriedDate , String username) throws Exception {
		try {
			tokenRecordRepository.update(expriedDate ,username);
		}catch(Exception ex) {
			throw new Exception(ex);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void delete(String username) throws Exception{
		tokenRecordRepository.deleteByUsername(username);
	}
	
	@Transactional(readOnly = true)
	public TokenRecord getOne(String username) {
		return tokenRecordRepository.getOne(username);
	}

}
