package com.crm.service.cust;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.common.PageResult;
import com.crm.model.entity.TbCust;
import com.crm.model.entity.TbCustPK;
import com.crm.model.view.cust.QueryForm;
import com.crm.model.view.cust.UpdateForm;
import com.crm.repository.CustRepository;
import com.crm.utils.DataManager;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustService {

	@Autowired
	private CustRepository custRespository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	public Long getSequence() {
		// TODO Auto-generated method stub
		return Long.valueOf(custRespository.getTbCustSeq());
	}

	@Transactional(readOnly = true)
	public Long checkPersonIdByPersonId(String personId) {
		// TODO Auto-generated method stub
		return custRespository.checkPersonIdByPersonId(personId);
	}

	@Transactional(readOnly = true)
	public List<TbCust> getList() {
		// TODO Auto-generated method stub
		return custRespository.findAll();
	}

	public List<TbCust> getList(Map<String, Object> params, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(rollbackFor = Exception.class)
	public TbCust saveAndGetPk(TbCust t) throws Exception {
		// TODO Auto-generated method stub
		try {

			TbCustPK tbCustPK = t.getId();
			tbCustPK.setCustId(String.valueOf(this.getSequence()));
			t.setId(tbCustPK);

			return custRespository.saveAndFlush(t);
		} catch (Exception e) {
			throw new Exception(e.getMessage(),e);
		}
	}

	@Transactional(readOnly = true)
	public Page<TbCust> getList(QueryForm custForm, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return custRespository.findAll(pageRequest);
	}

	@Transactional(readOnly = true)
	public PageResult<List<TbCust>> getSql(QueryForm custForm, PageRequest pageRequest) {
		// TODO Auto-generated method stub

		DataManager genSql1 = new DataManager(entityManager);
		genSql1.setModel(TbCust.class);
		genSql1.setPageRequest(pageRequest);
		genSql1.add("SELECT * FROM TB_CUST where 1=1 ");
		genSql1.add("and CUST_ID = :custID", custForm.getCustId());
		genSql1.add("and REGION_ID = :regionId", custForm.getRegionId());

		return (PageResult<List<TbCust>>) genSql1.getPageResult();

	}

	@Transactional(rollbackFor = Exception.class)
	public TbCust update(UpdateForm custUpdateForm) {
		// TODO Auto-generated method stub
		TbCustPK id = new TbCustPK();
		id.setCustId(custUpdateForm.getCustId());
		id.setRegionId(custUpdateForm.getRegionId());
		TbCust cust = custRespository.getOne(id);

		if (!StringUtils.isBlank(custUpdateForm.getCustLevel())) {
			cust.setCustLevel(custUpdateForm.getCustLevel());
		}
		if (!StringUtils.isBlank(custUpdateForm.getEngName())) {
			cust.setEngName(custUpdateForm.getEngName());
		}
		if (!StringUtils.isBlank(custUpdateForm.getLocName())) {
			cust.setLocName(custUpdateForm.getLocName());
		}
		if (!StringUtils.isBlank(custUpdateForm.getPersonType())) {
			cust.setPersonType(custUpdateForm.getPersonType());
		}
		if (!StringUtils.isBlank(custUpdateForm.getPersonId())) {
			cust.setPersonId(custUpdateForm.getPersonId());
		}
		return custRespository.save(cust);
	}

	public long checkPersonIdByPersonIdNotIn(String personId, String custId, String regionId) {
		// TODO Auto-generated method stub
		return custRespository.checkPersonIdByPersonIdNotIn(personId, custId, regionId);
	}

	@Transactional(readOnly = true)
	public PageResult<List<Map<String, Object>>> getSqlMap(QueryForm custForm, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}
}
