package com.crm.service.loginHistory;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.LoginHistory;
import com.crm.model.entity.LoginHistoryPk;
import com.crm.repository.LoginHistoryRepository;

@Service
public class LoginHistoryService {

	@Autowired
	private LoginHistoryRepository loginHistoryRepository;

	@Transactional(noRollbackFor = Exception.class)
	public void save(String username, boolean success,String reason) {
		LoginHistory loginHistory = new LoginHistory();
		LoginHistoryPk loginHistoryPk = new LoginHistoryPk();
		loginHistoryPk.setUsername(username);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
		Date current = new Date();
		loginHistoryPk.setLoginTime(sdf.format(current));
		loginHistory.setId(loginHistoryPk);
		loginHistory.setSuccess(success);
		loginHistory.setReason(reason);
		loginHistoryRepository.save(loginHistory);
	}

	
	
}
