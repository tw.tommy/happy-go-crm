package com.crm.service.api.permission.setting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.ApiPermissionSetting;
import com.crm.repository.ApiPermissionSettingRepository;
 
@Service
public class ApiPermissionSettingService {

	@Autowired
	private ApiPermissionSettingRepository apiPermissionSettingRepository;

	@Transactional(readOnly = true)
	public List<String> getCheckApi(String role) {
		return apiPermissionSettingRepository.getCheckApi(role);
	}

	@Transactional(readOnly = true)
	public List<ApiPermissionSetting> findAll() {
		return apiPermissionSettingRepository.findAllEnableRole();
	}

}
