package com.crm.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Value Converter
 *
 */
@Documented
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ValueConverter {

	/**
	 * 欄位名
	 */
	String field();

	/**
	 * Converter Name <br/>
	 * 若是未填寫, 則預設為 field()回傳值 <字首轉為大寫>
	 */
	String name() default "";

}
