package com.crm.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.AbstractRequestLoggingFilter;

public class MyRequestLoggingFilter extends AbstractRequestLoggingFilter {

	 
	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		// TODO Auto-generated method stub
		
		logger.error(message);
	}

	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		// TODO Auto-generated method stub
		logger.error(message);
	}

}
