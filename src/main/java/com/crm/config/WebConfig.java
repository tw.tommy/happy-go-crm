package com.crm.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.crm.interceptor.LogRequestInterceptor;
import com.p6spy.engine.spy.P6DataSource;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

	@Autowired
	private LogRequestInterceptor logRequestInterceptor;

//	@Override
//	protected void addInterceptors(InterceptorRegistry registry) {
//		// TODO Auto-generated method stub
//		registry.addInterceptor(logRequestInterceptor).addPathPatterns("/**");
//		//super.addInterceptors(registry);
//	}

	@Override
	protected void addCorsMappings(CorsRegistry registry) {
		// TODO Auto-generated method stub
		registry.addMapping("/**").allowedOrigins("*").allowedMethods("*").allowedHeaders("*").allowCredentials(true)
				.exposedHeaders("Content-disposition", "Filename");
	}

//	@Bean
//	public CommonsRequestLoggingFilter requestLoggingFilter() {
//		CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
//		loggingFilter.setIncludeClientInfo(true);
//		loggingFilter.setIncludeQueryString(true);
//		loggingFilter.setIncludePayload(true);
//		loggingFilter.setIncludeHeaders(true);
//		loggingFilter.setMaxPayloadLength(64000);
//		return loggingFilter;
//	}
//	
	@Bean
	public MyRequestLoggingFilter requestLoggingFilter() {
		MyRequestLoggingFilter loggingFilter = new MyRequestLoggingFilter();
		loggingFilter.setIncludeClientInfo(true);
		loggingFilter.setIncludeQueryString(true);
		loggingFilter.setIncludePayload(true);
		loggingFilter.setIncludeHeaders(true);
		loggingFilter.setMaxPayloadLength(64000);
		return loggingFilter;
	}

	@Profile("prod")
	@Bean
	public DataSource p6DataSource() {
		P6DataSource p6 = new P6DataSource();
		p6.setRealDataSource("jboss/MyOracle");
		return p6;
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html")
	      .addResourceLocations("classpath:/META-INF/resources/");
	    registry.addResourceHandler("/webjars/**")
	      .addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	

}
