package com.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class WebFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFrameworkApplication.class, args);
	}

}
