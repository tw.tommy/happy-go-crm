package com.crm.model.converter;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.crm.common.AbstractModelMapper;
import com.crm.model.entity.TbCust;
import com.crm.model.view.adjust.ListVo;

@Component
public class AdjustConverter extends AbstractModelMapper<TbCust, ListVo> {

	@Override
	public ListVo doConvert(TbCust source) {
		ModelMapper modelMapper = new ModelMapper();

		if (StringUtils.isBlank(source.getCustId())) {
			source.setCustId(source.getId().getCustId());
		}

		if (StringUtils.isBlank(source.getRegionId())) {
			source.setRegionId(source.getId().getRegionId());
		}

		source.setRegionIdName(ResourceConverter.region.get(source.getRegionId()));
		source.setCustLevelName(ResourceConverter.custLevel.get(source.getCustLevel()));
		return modelMapper.map(source, ListVo.class);
	}
}
