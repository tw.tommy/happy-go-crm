package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiPermissionSetting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7318715306370019650L;
	
	@EmbeddedId
	private ApiPermissionSettingPk id;
	
}
