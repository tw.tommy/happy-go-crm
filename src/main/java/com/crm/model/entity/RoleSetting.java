package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.crm.converter.RoleConverter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleSetting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -972855124415260716L;

	@Id
	private String role;
	
	private String name;
	
	@Transient
	private PathViewPermissionRelPk id;
	
	@Convert(converter = RoleConverter.class )
	@Column(name = "role",insertable = false , updatable = false)
	private String roleName;
	
	@Convert(converter = RoleConverter.class )
	private String def;
	 
	private boolean enabled;
}
