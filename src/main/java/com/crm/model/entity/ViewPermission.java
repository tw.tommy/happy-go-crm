package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewPermission implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -4368799316776940878L;

	@Id
	private String viewPermission;
	
	private String name;
	
	private String def;
}
