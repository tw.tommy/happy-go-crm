package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PathViewPermissionRel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3719826356975929785L;

	@EmbeddedId
	private PathViewPermissionRelPk id;
	
}
