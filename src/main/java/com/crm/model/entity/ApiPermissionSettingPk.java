package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ApiPermissionSettingPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4373211011907679730L;

	private String path;
	
	private String role;
}
