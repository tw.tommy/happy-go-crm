package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class TokenRecord implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 3419412111518148350L;
	
	@Id
	private String username;
	
	private String token;
	
	private String expriedDate;
	
	private String createDate;
	
}
