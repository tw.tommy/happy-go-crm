package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PathViewPermissionRelPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1844689232182064130L;
	private String path;
	private String viewPermission;
}
