package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

 

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathSetting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9044037193044291841L;

	@Id
	private String id;
	
	private String name;
	
	private String path;
	
	private String title;
	
	private String icon;
	
	private int tier;
	
	private String component;
	
	@Column(updatable = false , insertable = false)
	private int sort;
	
	private String redirect;
	private String parent;
	
	private boolean hidden;
	
}
