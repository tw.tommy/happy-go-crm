package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9150442751399532994L;

	@NotBlank(message = "大頭照不能為空!")
	private String image;
	
	@NotBlank(message = "姓名不能為空!")
	private String name;
	
	@Id
	@NotBlank(message = "ID不能為空!")
	private String username;
	
	@Email(message = "Email格式錯誤!")
	@NotBlank(message = "email不能為空!")
	private String email;
	
	@NotBlank(message = "性別不能為空!")
	private String sex;
	
	@NotBlank(message = "部門不能為空!")
	private String department;
	
	@Column(updatable = false)
	private String createDate;
	@Column(updatable = false)
	private String creater;
}
