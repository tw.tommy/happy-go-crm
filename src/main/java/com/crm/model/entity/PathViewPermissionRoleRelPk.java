package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathViewPermissionRoleRelPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7950944766166793710L;

	private String path;
	
	private String viewPermission;
	
	private String role;
	
}
