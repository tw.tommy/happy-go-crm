package com.crm.model.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathViewPermissionRoleRel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7226952428759211544L;

	@EmbeddedId
	private PathViewPermissionRoleRelPk id;
}
