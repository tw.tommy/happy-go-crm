package com.crm.model.view.admin.user;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class UserProfileDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6296279060033655111L;

	@NotBlank(message = "大頭照不能為空!")
	private String image;
	
	@NotBlank(message = "姓名不能為空!")
	private String name;
	
	@NotBlank(message = "email不能為空!")
	private String email;
	
	@NotBlank(message = "性別不能為空!")
	private String sex;
	
	@NotBlank(message = "所屬部門不能為空!")
	private String department;
	
}
