package com.crm.model.view.admin.user;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UsersDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8753512210895272832L;
	
	@NotBlank(message = "用戶ID不能為空!")
	private String username;
	
	@NotNull(message = "狀態不能為空!")
	private boolean enable;
	
}
