package com.crm.jwt.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.crm.common.HttpStatus;
import com.crm.jwt.JwtTokenUtil;
import com.crm.jwt.JwtUserDetailsService;
import com.crm.model.entity.TokenRecord;
import com.crm.service.TokenRecordService.TokenRecordSercvice;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private TokenRecordSercvice tokenRecordSercvice;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		final String requestTokenHeader = request.getHeader("Authorization");

		String username = null;
		String jwtToken = null;

		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				username = jwtTokenUtil.getUsernameFromToken(jwtToken);// INVALID_CREDENTIALS
			} catch (IllegalArgumentException e) {
				log.info("Unable to get JWT Token");
			} catch (ExpiredJwtException e) {
				log.info("JWT Token has expired");
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = mapper.createObjectNode();
				json.put("status", HttpStatus.EXCEPTION.toString());
				json.put("message", "TOKEN_EXPIRED");
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(json.toString());
				response.getWriter().flush();
				response.getWriter().close();
				return;
			} catch (MalformedJwtException e) {
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = mapper.createObjectNode();
				json.put("status", HttpStatus.EXCEPTION.toString());
				json.put("message", e.getMessage());
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().write(json.toString());
				response.getWriter().flush();
				response.getWriter().close();
				return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			logger.warn("JWT Token does not begin with Bearer String");
		}

		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null && !username.equals("SBAadmin")) {
			UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
			try {
				if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {

					TokenRecord tokenRecord = tokenRecordSercvice.getOne(username);

					if (null == tokenRecord) {
						log.error("JWT is not exists in database");
						ObjectMapper mapper = new ObjectMapper();
						ObjectNode json = mapper.createObjectNode();
						json.put("status", HttpStatus.EXCEPTION.toString());
						json.put("message", "TOKEN_MISSING");
						response.setStatus(HttpServletResponse.SC_OK);
						response.setContentType("application/json;charset=UTF-8");
						response.getWriter().write(json.toString());
						response.getWriter().flush();
						response.getWriter().close();
						return;
					}

					// Token一樣就更新過期時間 否則出現response錯誤
					if (tokenRecord.getToken().equals(jwtToken)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");

						Date expriedDate = sdf.parse(tokenRecord.getExpriedDate());
						Date now = new Date();

						if (now.after(expriedDate)) {
							log.info("JWT Token has expired");
							ObjectMapper mapper = new ObjectMapper();
							ObjectNode json = mapper.createObjectNode();
							json.put("status", HttpStatus.EXCEPTION.toString());
							json.put("message", "TOKEN_EXPIRED");
							response.setStatus(HttpServletResponse.SC_OK);
							response.setContentType("application/json;charset=UTF-8");
							response.getWriter().write(json.toString());
							response.getWriter().flush();
							response.getWriter().close();
							return;
						}
						tokenRecord.setCreateDate(sdf.format(now));
						Date after = new Date();
						after = DateUtils.addHours(after, 2);
						tokenRecordSercvice.update(sdf.format(after), username);
					} else {
						log.info("other device has been logged");
						ObjectMapper mapper = new ObjectMapper();
						ObjectNode json = mapper.createObjectNode();
						json.put("status", HttpStatus.EXCEPTION.toString());
						json.put("message", "OTHER_LOGIN");
						response.setStatus(HttpServletResponse.SC_OK);
						response.setContentType("application/json;charset=UTF-8");
						response.getWriter().write(json.toString());
						response.getWriter().flush();
						response.getWriter().close();
						return;
					}

					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities());
					usernamePasswordAuthenticationToken
							.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

//		if (username.equals("SBAadmin")) {
//
//			List<GrantedAuthority> grantedAuthority = new ArrayList<GrantedAuthority>();
//
//			grantedAuthority.add(new SimpleGrantedAuthority("SBA_ROLE"));
//
//			User user = new User("SBAadmin", "SBAadmin", true, true, true, true, grantedAuthority);
//			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//					user, null, user.getAuthorities());
//			usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//		}

		filterChain.doFilter(request, response);

	}

}
