package com.crm.jwt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import com.crm.model.entity.ApiPermissionSetting;
import com.crm.model.entity.ApiPermissionSettingPk;
import com.crm.service.api.permission.setting.ApiPermissionSettingService;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	private final AntPathMatcher antPathMatcher = new AntPathMatcher();

	private final String[] exeludePaths = { "/authenticate", "/getUserInfo", "/user/logout" };

	private final String[] adminPaths = { "/actuator", "/instances" };

	private final String[] webSocketPaths = { "/myUrl" };

	private final String[] swaggerPaths = { "/swagger-ui.html", "/webjars" ,"/swagger-resources","/swagger-resources" ,"/v2/api-docs"};

	private final List<String> exeludePathList = Arrays.asList(exeludePaths);

	@Autowired
	private ApiPermissionSettingService apiPermissionSettingService;

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		FilterInvocation fi = (FilterInvocation) object;
		String url = fi.getRequestUrl();
		if (url.contains("?")) {
			url = url.substring(0, url.indexOf("?"));
		}
		List<ApiPermissionSetting> apiList = apiPermissionSettingService.findAll();

		List<String> urlRoles = new ArrayList<>();

		for (ApiPermissionSetting index : apiList) {
			if (antPathMatcher.match(index.getId().getPath(), url)) {
				urlRoles.add(index.getId().getRole());
			}
		}

		if (!urlRoles.isEmpty()) {
			String[] arr = new String[urlRoles.size()];
			urlRoles.toArray(arr);
			return SecurityConfig.createList(arr);
		}

		if (exeludePathList.contains(url)) {
			return null;
		}

		// 排除admin monior 角色驗證
		for (String ignorePath : adminPaths) {
			if (url.startsWith(ignorePath)) {
				return null;
			}
		}
		// 排除web socket 角色驗證
		for (String ignorePath : webSocketPaths) {
			if (url.startsWith(ignorePath)) {
				return null;
			}
		}

		// 排除swagger 角色驗證
		for (String ignorePath : swaggerPaths) {
			if (url.startsWith(ignorePath)) {
				return null;
			}
		}

		return SecurityConfig.createList("BASIC", "SBA_ROLE");
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return FilterInvocation.class.isAssignableFrom(clazz);
	}

}
