package com.crm.jwt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.jwt.model.Roles;
import com.crm.jwt.model.RolesPk;

@Repository("jwtRolesRepository")
public interface RolesRepository extends JpaRepository<Roles, RolesPk> {
	
	@Query(nativeQuery = true , value = "SELECT * FROM ROLES WHERE EXISTS(SELECT 1 FROM ROLE_SETTING WHERE ROLE_SETTING.ROLE = ROLES.ROLE AND ENABLED = 1) AND USERNAME=:username")
	public List<Roles> findRoleByUsername(@Param("username") String username);
}
