package com.crm.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.jwt.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, String> {

	@Modifying
	@Query(value="UPDATE　USERS SET ENABLE = :enable WHERE USERNAME = :username",nativeQuery = true)
	public void updateEnabledByUsername(@Param("enable") boolean enable , @Param("username") String username);
	
}
