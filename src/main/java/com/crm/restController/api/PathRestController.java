package com.crm.restController.api;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.model.entity.PathSetting;
import com.crm.service.path.setting.PathSettingService;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/setting/path")
@Slf4j
public class PathRestController {

	@Autowired
	private PathSettingService settingPathService;

	@GetMapping("/url")
	public ResponseEntity<Result<?>> url() throws Exception {
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "查詢成功!",
				settingPathService.getURL(UserProfileUtils.getRoles()).toString()));
	}

	@GetMapping("/getAllPath")
	public ResponseEntity<Result<String>> getAllPath(@RequestParam(required = false) String param) throws Exception {
		if (!StringUtils.isBlank(param)) {
			return ResponseEntity
					.ok(new Result<String>(HttpStatus.OK, "查詢成功!", settingPathService.getAllPath(param).toString()));
		}
		return ResponseEntity
				.ok(new Result<String>(HttpStatus.OK, "查詢成功!", settingPathService.getAllPath().toString()));
	}

	@GetMapping("/getAllPathAndTitle")
	public ResponseEntity<Result<List<Map<String, Object>>>> getAllPathAndTitle() {
		return ResponseEntity.ok(new Result<List<Map<String, Object>>>(HttpStatus.OK, "查詢成功!",
				settingPathService.getFullPathAndTitle()));
	}

	@GetMapping("/getOneTier")
	public ResponseEntity<Result<List<PathSetting>>> getOneTier() {
		return ResponseEntity
				.ok(new Result<List<PathSetting>>(HttpStatus.OK, "查詢成功!", settingPathService.getOneTier()));
	}

	@GetMapping("/getTree")
	public ResponseEntity<Result<String>> getTree(@RequestParam String id) {
		String path = settingPathService.getTree(id).toString();
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "查詢成功!", path));
	}

	@GetMapping("/getMenuByParent")
	public ResponseEntity<Result<List<PathSetting>>> getMenuByParent(@RequestParam String parent,
			@RequestParam int tier) {
		return ResponseEntity.ok(new Result<List<PathSetting>>(HttpStatus.OK, "查詢成功!",
				settingPathService.getMenuByParent(parent, tier)));
	}

	@GetMapping("/getPathByParent")
	public ResponseEntity<Result<List<PathSetting>>> getPathByParent(String oneTierMenu,
			@RequestParam(required = false) String twoTierMenu, @RequestParam(required = false) String threeTierMenu) {
		return ResponseEntity.ok(new Result<List<PathSetting>>(HttpStatus.OK, "查詢成功!",
				settingPathService.getPathByParent(oneTierMenu, twoTierMenu, threeTierMenu)));
	}

}
