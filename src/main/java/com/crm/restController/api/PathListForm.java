package com.crm.restController.api;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathListForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 968079253744501177L;
	
	@Valid
	private List<PathForm> pathForm;
}
