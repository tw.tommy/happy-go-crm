package com.crm.restController.api;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 863856234935776431L;

	@NotBlank(message = "請輸入路徑!")
	private String path;
	
	@NotBlank(message = "請輸入標題!")
	private String title;
	
	@NotBlank(message = "請選擇ICON!")
	private String icon;
}
