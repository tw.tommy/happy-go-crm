package com.crm.restController.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.model.entity.RoleSetting;
import com.crm.service.role.setting.RoleSettingService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/setting/role")
@Slf4j
public class RoleRestController {

	@Autowired
	private RoleSettingService settingRoleService;

	@GetMapping("/findAll")
	public ResponseEntity<Result<List<RoleSetting>>> findAll() {
		return ResponseEntity.ok(new Result<List<RoleSetting>>(HttpStatus.OK, "查詢成功!", settingRoleService.findAll()));
	}

}
