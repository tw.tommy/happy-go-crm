package com.crm.restController.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.model.entity.ViewPermission;
import com.crm.service.view.permission.ViewPermissionService;

@RestController
@RequestMapping("/setting/funcPermission")
public class FuncPermissionRestController {

	@Autowired
	private ViewPermissionService settingFuncPermissionService;

	@GetMapping("/list")
	public ResponseEntity<Result<?>> findAll() throws Exception {
		List<ViewPermission> list = settingFuncPermissionService.findAll();

		if (list.isEmpty()) {
			return ResponseEntity.ok(new Result<List<ViewPermission>>(HttpStatus.OK, "查無資料!"));
		}
		return ResponseEntity.ok(new Result<List<ViewPermission>>(HttpStatus.OK, "查詢成功!", list));
	}

}
