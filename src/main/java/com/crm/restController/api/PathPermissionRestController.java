package com.crm.restController.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.service.path.view.permission.role.rel.PathViewPermissionRoleRelService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/setting/pathPermission")
@Slf4j
public class PathPermissionRestController {

	@Autowired
	private PathViewPermissionRoleRelService settingPathPermissionService;

	@GetMapping("getIdByRole")
	public ResponseEntity<Result<List<String>>> getIdByRole(@RequestParam String role) {
		return ResponseEntity
				.ok(new Result<List<String>>(HttpStatus.OK, "查詢成功!", settingPathPermissionService.getIdByRole(role)));
	}

}
