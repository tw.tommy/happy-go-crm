package com.crm.restController.basic.adjust;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.PageResult;
import com.crm.common.Result;
import com.crm.model.entity.TbCust;
import com.crm.model.view.adjust.ListVo;
import com.crm.model.view.adjust.QueryForm;
import com.crm.model.view.adjust.UpdateForm;
import com.crm.service.BaseService;
import com.crm.service.basic.adjust.AdjustService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/basic/adjust/manager")
@Slf4j
public class AdjustRestController extends BaseService {

	@Autowired
	private AdjustService adjustService;

	// 查詢列表頁資料
	@GetMapping("/index/query")
	public ResponseEntity<?> query(QueryForm form, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) {
		PageResult<ListVo> pr = null;

		try {
			pr = adjustService.getListData(form, current, pageSize);
		} catch (Exception e) {
			log.error(e.getMessage());
			pr = new PageResult<ListVo>();
			pr.setStatus(HttpStatus.ERROR, MSG_QUERY_FAIL);
		}

		return ResponseEntity.ok(pr);
	}

	// TODO: 進明細頁要撈一次資料
	// 查詢明細頁資料

	// 新增
	@PostMapping("/detail/add")
	public ResponseEntity<Result<String>> save(@RequestBody @Valid TbCust cust, BindingResult br) {
		Result<String> result = new Result<String>(br);

		try {
			// 檢核
			if (!br.hasErrors()) {
				if (adjustService.countByPersonId(cust.getPersonId()) > 0) {
					result.setStatus(HttpStatus.ERROR, "身分證已存在");
				}
			}

			if (result.getStatus() == HttpStatus.OK) {
				TbCust tbCustResult = adjustService.add(cust);
				result.setData(tbCustResult.getId().getCustId());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			result.setStatus(HttpStatus.ERROR, MSG_ADD_FAIL);
		}

		return ResponseEntity.ok(result);
	}

	// 修改
	@PostMapping("/detail/update")
	public ResponseEntity<?> update(@RequestBody @Valid UpdateForm form, BindingResult br) {
		Result<String> result = new Result<String>(br);

		try {
			// 檢核
			if (!br.hasErrors()) {
				if (adjustService.countByPersonId(form.getPersonId()) > 1) {
					result.setErrorStatus("身分證已存在");
				}
			}

			if (result.getStatus() == HttpStatus.OK) {
				adjustService.update(form);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			result.setStatus(HttpStatus.ERROR, MSG_UPDATE_FAIL);
		}

		return ResponseEntity.ok(result);
	}

	// 刪除
	@PostMapping("/detail/delete")
	public ResponseEntity<?> delete(@RequestBody @Valid UpdateForm form, BindingResult br) {
		Result<String> result = new Result<String>(br);

		try {
			if (result.getStatus() == HttpStatus.OK) {
				adjustService.delete(form);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			result.setStatus(HttpStatus.ERROR, MSG_DELETE_FAIL);
		}

		return ResponseEntity.ok(result);
	}
}
