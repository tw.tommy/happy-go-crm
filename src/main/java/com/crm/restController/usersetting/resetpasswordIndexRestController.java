package com.crm.restController.usersetting;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.service.users.UsersService;

@RestController
@RequestMapping("/usersetting/resetpassword/function")
public class resetpasswordIndexRestController {

	@Autowired
	private UsersService usersService;
	
	@PutMapping
	public ResponseEntity<Result<String>> update(@RequestBody @Valid Form form, BindingResult br) throws Exception {

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR,sb.toString()));
		}
		usersService.update(form);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "更新成功!"));
	}

}
