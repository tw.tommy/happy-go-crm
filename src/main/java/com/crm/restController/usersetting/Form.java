package com.crm.restController.usersetting;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Form implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -2368149018200708536L;

	@NotBlank(message="請輸入目前密碼")
	private String currentPassword;

	@NotBlank(message="請輸入新密碼")
	private String newPassword;
	
}
