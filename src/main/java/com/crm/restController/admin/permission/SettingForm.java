package com.crm.restController.admin.permission;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettingForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1038650842765204025L;

	@NotBlank(message = "請選擇角色!")
	private String role;
	
	@NotBlank(message = "請選擇主標題!")
	private String title;
	
	private List<String> paths;
	
	private List<String> excludePaths;
}
