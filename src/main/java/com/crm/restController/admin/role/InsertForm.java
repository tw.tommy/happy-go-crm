package com.crm.restController.admin.role;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3050456004247406151L;

	@NotBlank(message = "角色代號不能為NULL!")
	private String role;

	@NotBlank(message = "角色名稱不能為NULL!")
	private String name;

	private String def;

	@NotBlank(message = "請選擇激活狀態!")
	private boolean enabled;
}
