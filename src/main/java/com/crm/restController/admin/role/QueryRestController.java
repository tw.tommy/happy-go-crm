package com.crm.restController.admin.role;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.ReportType;
import com.crm.common.Result;
import com.crm.model.entity.RoleSetting;
import com.crm.service.role.setting.RoleSettingService;
import com.crm.utils.ReportUtils;

@RestController
@RequestMapping("/admin/role/query")
public class QueryRestController {

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private RoleSettingService roleSettingService;

	@GetMapping("/index/query")
	public ResponseEntity<Result<?>> query(@RequestParam String enable) throws Exception {
		return ResponseEntity.ok(
				new Result<List<RoleSetting>>(HttpStatus.OK, "查詢成功!", roleSettingService.findAllByEnabled(enable)));
	}

	@PostMapping("/index/update")
	public ResponseEntity<Result<?>> update(@RequestBody InsertForm form, BindingResult br) throws Exception {
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		roleSettingService.upate(form);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "更新成功!"));
	}

	@DeleteMapping("/index/delete")
	public ResponseEntity<Result<?>> delete(@RequestParam String role) throws Exception {
		roleSettingService.delete(role);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "刪除成功!"));
	}

	@GetMapping("/index/report")
	public void getReport(@RequestParam String enable) throws Exception {
		List<RoleSetting> lists = roleSettingService.findAllByEnabled(enable);
		try {
			ReportUtils ru = new ReportUtils();
			ru.setResponse(response);
			ru.setFields(lists);
			ru.setJrxmlName("role");
			ru.setReportName("role");
			ru.setReportType(ReportType.PDF.toString());
			ru.genReport();
		} catch (Exception ex) {
			throw ex;
		}

	}

}
