package com.crm.restController.admin.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.service.role.setting.RoleSettingService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin/role/insert")
@Slf4j
public class InsertRestController {

	@Autowired
	private RoleSettingService roleSettingService;

	@PostMapping("/index/add")
	public ResponseEntity<Result<?>> add(@RequestBody InsertForm form, BindingResult br) throws Exception {
		log.info("form " + form);
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		roleSettingService.save(form);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));

	}

}
