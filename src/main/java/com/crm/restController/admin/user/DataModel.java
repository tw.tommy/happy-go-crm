package com.crm.restController.admin.user;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.crm.model.view.admin.user.UserProfileDto;
import com.crm.model.view.admin.user.UsersDto;

import lombok.Data;

@Data
public class DataModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5770813965076039105L;

	@Valid
	private UserProfileDto userProfile;
	
	@Valid
	private UsersDto users;
	
	@Size(min = 1,message = "權限不能為空!")
	private List<String> roles;
}
