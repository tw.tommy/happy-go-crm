package com.crm.restController.admin.user;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.ResponseMap;
import com.crm.common.Result;
import com.crm.jwt.model.Users;
import com.crm.model.entity.UserProfile;
import com.crm.model.view.admin.user.UserProfileDto;
import com.crm.model.view.admin.user.UsersDto;
import com.crm.service.roles.RolesService;
import com.crm.service.userProfile.UserProfileService;
import com.crm.service.users.UsersService;
import com.crm.utils.UserProfileUtils;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin/user/mgu")
@Slf4j
public class UserMguController {

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private RolesService rolesService;
	
	@PostMapping("/index/add")
	public ResponseEntity<Result<String>> add(@RequestBody @Valid UserRegisterForm userRegisterForm, BindingResult br)
			throws Exception {

		log.info("LoginUser " + UserProfileUtils.getUsername());
		log.info("userRegisterForm " + userRegisterForm.toString());
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		try {
			userProfileService.save(userRegisterForm);
			return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));
		} catch (Exception ex) {
			throw new Exception("新增失敗!");
		}

	}

	@GetMapping("/index/list")
	public ResponseEntity<Result<List<Map<String, Object>>>> query(@RequestParam Map<String, Object> map) {
		List<Map<String, Object>> lists = userProfileService.getList(map);
		return ResponseMap.getResult(lists);
	}

	@PutMapping("/index")
	public ResponseEntity<Result<String>> update(@RequestBody @Valid DataModel dataModel, BindingResult br) throws Exception {
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		//更新用戶狀態
		usersService.updateEnableByUsername(dataModel.getUsers().isEnable(), dataModel.getUsers().getUsername());
		
		//更新用戶profile
		UserRegisterForm userRegisterForm = new UserRegisterForm();
		userRegisterForm.setImage(dataModel.getUserProfile().getImage());
		userRegisterForm.setName(dataModel.getUserProfile().getName());
		userRegisterForm.setDepartment(dataModel.getUserProfile().getDepartment());
		userRegisterForm.setSex(dataModel.getUserProfile().getSex());
		userRegisterForm.setEmail(dataModel.getUserProfile().getEmail());
		userRegisterForm.setUsername(dataModel.getUsers().getUsername());
		userRegisterForm.setRoles(dataModel.getRoles());
		userProfileService.update(userRegisterForm);
		
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));

	}

	@GetMapping("/index/{username}")
	public ResponseEntity<Result<Object>> findByUsername(
			@PathVariable(value = "username", required = true) String username) {

		UserProfile userProfile = userProfileService.findByUsername(username);

		Users users = usersService.findUserById(username);

		if (null != users.getPassword()) {
			users.setPassword(null);
		}

		ModelMapper model = new ModelMapper();

		UserProfileDto userProfileDto = model.map(userProfile, UserProfileDto.class);

		UsersDto usersDto = model.map(users, UsersDto.class);

		List<String> roles = rolesService.findRoleById(username);
		
		Map<String, Object> map = Map.of("userProfile", userProfileDto, "users", usersDto,"roles",roles);

		return ResponseEntity.ok(new Result<Object>(HttpStatus.OK, "查詢成功", map));
	}

}
