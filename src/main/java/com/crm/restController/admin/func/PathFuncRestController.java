package com.crm.restController.admin.func;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.service.path.view.permission.rel.PathViewPermissionRelService;

@RestController
@RequestMapping("/admin/func/pathFunc")
public class PathFuncRestController {

	@Autowired
	private PathViewPermissionRelService pathViewPermissionRelService;

	@PostMapping("/index/add")
	public ResponseEntity<Result<?>> add(@RequestBody @Valid PathFuncAddForm form, BindingResult br) throws Exception {

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}

		pathViewPermissionRelService.save(form);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));

	}
	
	@GetMapping("/index/")
	public ResponseEntity<Result<Object>> get(@RequestParam String path){
		return ResponseEntity.ok(new Result<Object>(HttpStatus.OK,"查詢成功!",pathViewPermissionRelService.findviewPermissionByPath(path)));
	}

}
