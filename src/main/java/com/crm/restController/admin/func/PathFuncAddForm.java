package com.crm.restController.admin.func;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PathFuncAddForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2596940435730540467L;

	@NotBlank(message = "請輸入路徑!")
	private String path;
	
	@Size(min=1, message = "請選擇功能權限!")
	private List<String> viewPermission;

}
