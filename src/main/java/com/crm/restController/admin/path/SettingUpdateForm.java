package com.crm.restController.admin.path;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettingUpdateForm implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = -9162941259709625290L;

	private String id;

	@NotBlank(message = "標題不能為空!")
	private String title;
	
	@NotBlank(message = "請選擇ICON!")
	private String icon;

}
