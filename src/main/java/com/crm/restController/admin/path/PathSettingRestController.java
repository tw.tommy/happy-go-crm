package com.crm.restController.admin.path;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.restController.api.PathForm;
import com.crm.service.path.setting.PathSettingService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin/path/setting")
@Slf4j
public class PathSettingRestController {

	@Autowired
	private PathSettingService pathSettingService;

	@PostMapping("/index/add")
	public ResponseEntity<Result<?>> add(@RequestBody @Valid List<PathForm> pathForm, BindingResult br)
			throws Exception {
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}

		pathSettingService.save(pathForm);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));

	}

	@PostMapping("/index/update")
	public ResponseEntity<Result<?>> update(@RequestBody @Valid SettingUpdateForm updateForm, BindingResult br)
			throws Exception {
		log.info("updateForm " + updateForm.toString());
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		pathSettingService.updatePath(updateForm);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "更新成功"));

	}

	@PostMapping("/index/updateRedirect")
	public ResponseEntity<Result<?>> updateRedirect(@RequestBody @Valid SettingUpdateRedirectForm form,
			BindingResult br) throws Exception {
		log.info("form " + form.toString());

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}
		pathSettingService.updateRedirect(form);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "更新成功"));
	}

	@DeleteMapping("/index/delete")
	public ResponseEntity<Result<?>> delete(@RequestBody(required = true) @Valid String id, BindingResult br)
			throws Exception {
		pathSettingService.deletePath(id);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "刪除成功"));
	}

	@PostMapping("/index/updateSort")
	public ResponseEntity<Result<?>> updateSort(@RequestBody List<String> id) throws Exception {
		pathSettingService.updateSort(id);
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "新增成功!"));
	}

}
