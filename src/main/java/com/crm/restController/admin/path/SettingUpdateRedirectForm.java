package com.crm.restController.admin.path;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettingUpdateRedirectForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -374182824341746304L;

	@NotBlank(message = "ID不能為空!")
	private String id;
	
	@NotBlank(message = "Redirect不能為空!")
	private String redirect;
	
}
