package com.crm.restController.cust;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.PageResult;
import com.crm.common.Result;
import com.crm.model.converter.CustConverter;
import com.crm.model.entity.TbCust;
import com.crm.model.view.cust.ListVo;
import com.crm.model.view.cust.QueryForm;
import com.crm.model.view.cust.UpdateForm;
import com.crm.service.cust.CustService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/cust")
@Slf4j
public class CustRestController {

	@Autowired
	private CustService custService;

	@Autowired
	private CustConverter custConverter;

	@PostMapping("/add")
	public ResponseEntity<Result<String>> save(@RequestBody @Valid TbCust tbCust, BindingResult br) throws Exception {

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}

		long checkPersonID = custService.checkPersonIdByPersonId(tbCust.getPersonId());
		if (checkPersonID > 0) {
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, "身分證已存在!"));
		}

		try {
			TbCust tbCustResult = custService.saveAndGetPk(tbCust);
			log.info(tbCustResult.getId().getCustId());
			return ResponseEntity.ok(new Result<String>(HttpStatus.OK, tbCustResult.getId().getCustId()));
		} catch (Exception e) {
			throw new Exception("新增失敗!");
		}

	}

	@GetMapping(value = "/list")
	public ResponseEntity<?> list(QueryForm custForm, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) {

		List<ListVo> custDtoList = new ArrayList<ListVo>();

		PageRequest pageRequest = PageRequest.of(current - 1, pageSize);

		Page<TbCust> page = custService.getList(custForm, pageRequest);

		for (TbCust index : page.getContent()) {
			ListVo dto = custConverter.convert(index);
			custDtoList.add(dto);
		}

		PageResult<List<ListVo>> pr = new PageResult<List<ListVo>>();

		pr.setStatus(HttpStatus.OK);
		pr.setMessage("查詢成功!");
		pr.setData(custDtoList);
		pr.setCurrent(page.getNumber());
		pr.setTotal(page.getTotalElements());
		pr.setPageSize(page.getSize());
		return ResponseEntity.ok(pr);
	}

	@GetMapping("/lists")
	public ResponseEntity<?> lists(QueryForm custForm, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) {

		PageRequest pageRequest = PageRequest.of(current - 1, pageSize);
		List<ListVo> custDtoList = new ArrayList<ListVo>();

		PageResult<List<TbCust>> page = custService.getSql(custForm, pageRequest);

		if (null != page.getData()) {
			for (TbCust index : (List<TbCust>) page.getData()) {
				ListVo dto = custConverter.convert(index);
				custDtoList.add(dto);
			}
		}
		PageResult<List<ListVo>> pr = new PageResult<List<ListVo>>();

		pr.setStatus(HttpStatus.OK);
		pr.setMessage("查詢成功!");
		pr.setData(custDtoList);
		pr.setCurrent(page.getCurrent());
		pr.setTotal(page.getTotal());
		pr.setPageSize(page.getPageSize());
		return ResponseEntity.ok(pr);
	}

	@GetMapping("/listm")
	public ResponseEntity<?> listm(QueryForm custForm, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) {

		PageRequest pageRequest = PageRequest.of(current - 1, pageSize);

		PageResult<List<Map<String, Object>>> page = custService.getSqlMap(custForm, pageRequest);

		return ResponseEntity.ok(page);
	}

	@GetMapping(value = "/xml", produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<?> xml(QueryForm custForm, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) {

		PageRequest pageRequest = PageRequest.of(current - 1, pageSize);
		List<ListVo> custDtoList = new ArrayList<ListVo>();

		PageResult<List<TbCust>> page = custService.getSql(custForm, pageRequest);

		if (null != page.getData()) {
			for (TbCust index : (List<TbCust>) page.getData()) {
				log.info("index=" + index.toString());
				ListVo dto = custConverter.convert(index);
				custDtoList.add(dto);
			}
		}
		PageResult<List<ListVo>> pr = new PageResult<List<ListVo>>();

		pr.setStatus(HttpStatus.OK);
		pr.setMessage("查詢成功!");
		pr.setData(custDtoList);
		pr.setCurrent(page.getCurrent());
		pr.setTotal(page.getTotal());
		pr.setPageSize(page.getPageSize());
		return ResponseEntity.ok(pr);
	}

	@PostMapping("/modify")
	public ResponseEntity<?> modify(@RequestBody @Valid UpdateForm custUpdateForm, BindingResult br) {
		log.info("modify =>" + custUpdateForm.toString());
		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, sb.toString()));
		}

		long checkPersonID = custService.checkPersonIdByPersonIdNotIn(custUpdateForm.getPersonId(),
				custUpdateForm.getCustId(), custUpdateForm.getRegionId());
		if (checkPersonID > 0) {
			return ResponseEntity.ok(new Result<String>(HttpStatus.ERROR, "身分證已存在!"));
		}

		TbCust cust = custService.update(custUpdateForm);
		log.info("cust = " + cust.toString());
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "更新成功!"));
	}

}
