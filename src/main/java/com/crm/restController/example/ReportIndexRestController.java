package com.crm.restController.example;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.model.entity.RoleSetting;
import com.crm.service.role.setting.RoleSettingService;
import com.crm.utils.ReportUtils;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/example/report/function")
public class ReportIndexRestController {

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private RoleSettingService settingRoleService;

	@GetMapping
	public void pdf(@RequestParam(value = "fileType", defaultValue = "PDF") String fileType) {
		List<RoleSetting> lists = settingRoleService.findAllByEnabled("ALL");
		ReportUtils ru = new ReportUtils();
		ru.setResponse(response);
		ru.setFields(lists);
		ru.setJrxmlName("role");
		ru.setReportName("role");
		ru.setReportType(fileType);
		try {
			ru.genReport();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
