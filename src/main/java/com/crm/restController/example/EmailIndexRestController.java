package com.crm.restController.example;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.common.HttpStatus;
import com.crm.common.Result;
import com.crm.utils.EmailUtils;

@RestController
@RequestMapping("/example/email/function")
public class EmailIndexRestController {

	@Autowired
	private EmailUtils sender;

	@PostMapping
	public ResponseEntity<Result<String>> sendMail() throws MessagingException, IOException {
		sender.sendSimpleEmail("lovius@hywebgt.com", "測試", "測試123");
		//sender.sendAttachmentEmail("lovius@hywebgt.com", "測試", "測試123", "A.jpg");
		return ResponseEntity.ok(new Result<String>(HttpStatus.OK, "發送成功!"));
	}

}
