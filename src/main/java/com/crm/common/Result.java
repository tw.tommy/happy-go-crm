package com.crm.common;

import java.io.Serializable;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 196719912526387262L;

	public Result(BindingResult br) {
		Init();

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError item : br.getAllErrors()) {
				sb.append(item.getDefaultMessage() + "<br />");
			}

			this.status = HttpStatus.ERROR;
			this.setMessage(sb.toString());
		}
	}

	public Result(HttpStatus httpStatus, String message) {
		Init();
		this.status = httpStatus;
		this.message = message;
	}

	private void Init() {
		this.status = HttpStatus.OK;
	}

	public void setStatus(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	public void setErrorStatus() {
		this.status = HttpStatus.ERROR;
		this.message = "異動失敗";
	}

	public void setErrorStatus(String message) {
		this.status = HttpStatus.ERROR;
		this.message = message;
	}

	/**
	 * Requst 處理結果
	 */
	private HttpStatus status;

	/**
	 * 訊息
	 */
	private String message;

	/**
	 * 資料
	 */
	private Object data;

	/**
	 * 設定資料
	 */
	public void setContent(List<T> content) {
		this.data = content;
	};

	/**
	 * 取得資料
	 */
	@SuppressWarnings("unchecked")
	public List<T> getContent() {
		if (this.data instanceof List) {
			return (List<T>) this.data;
		} else {
			return null;
		}
	}

}
