package com.crm.common;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class oTo {

	public static Object fill(Object source, Class<?> produce) throws Exception {
		if (null == source) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Field[] sourceFields = source.getClass().getDeclaredFields();
		for (Field field : sourceFields) {
			field.setAccessible(true);
			if (field.getName().equals("serialVersionUID")) {
				continue;
			}
			map.put(field.getName(), field.get(source));
		}

		Class<?> clazz = Class.forName(produce.getName());
		Object classInit = clazz.newInstance();
		Field[] produceFields = classInit.getClass().getDeclaredFields();

		for (Field field : produceFields) {
			field.setAccessible(true);
			if (field.getName().equals("serialVersionUID")) {
				continue;
			}
			if (map.containsKey(field.getName())) {
				field.set(classInit, map.get(field.getName()));
			} else {
				if (field.getType().getTypeName().startsWith("com")) {
					Class<?> subClazz = Class.forName(field.getType().getName());
					Object subClassInit = subClazz.newInstance();
					Field[] subFields = subClassInit.getClass().getDeclaredFields();
					for (Field subField : subFields) {
						subField.setAccessible(true);
						if (subField.getName().equals("serialVersionUID")) {
							continue;
						}
						if (map.containsKey(field.getName())) {
							subField.set(subClassInit, map.get(field.getName()));
						}
					}
					field.set(classInit,subClassInit);
				}
			}
		}

		log.info("classInit " + classInit.toString());

		return classInit;
	}

}
