package com.crm.common;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JacksonXmlRootElement
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class PageResult<T> extends Result<T> implements Serializable {

	private static final long serialVersionUID = 196719912526387262L;

	/**
	 * 目前頁數
	 */
	private long current;

	/**
	 * 查詢結果總筆數
	 */
	private long total;

	/**
	 * 每頁幾筆
	 */
	private long pageSize;

}
