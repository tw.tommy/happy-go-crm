package com.crm.common;

public enum ReportType {
	PDF,
	XLSX,
	CSV,
	DOCX
}
