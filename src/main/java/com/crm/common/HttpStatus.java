package com.crm.common;

/**
 * 
 * Requst 處理結果
 */
public enum HttpStatus {
	/**
	 * 成功
	 */
	OK,
	/**
	 * 發生錯誤
	 */
	ERROR,
	/**
	 * 發生異常錯誤
	 */
	EXCEPTION
}