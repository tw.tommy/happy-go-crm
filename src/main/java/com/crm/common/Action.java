package com.crm.common;

/**
 * 定義使用者執行的操作
 *
 */
public enum Action {
	/**
	 * 新增
	 */
	ADD,
	/**
	 * 修改
	 */
	UPLDATE,
	/**
	 * 刪除
	 */
	DELETE,
	/**
	 * 新增/修改/刪除
	 */
	ALL
}
