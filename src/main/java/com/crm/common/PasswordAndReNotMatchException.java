package com.crm.common;

public class PasswordAndReNotMatchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2029036834337176671L;

	public PasswordAndReNotMatchException(String message) {
		super(message);
	}
	
}
