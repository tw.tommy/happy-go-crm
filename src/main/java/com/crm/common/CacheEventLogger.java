package com.crm.common;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CacheEventLogger implements CacheEventListener<Object, Object> {

	@Override
	public void onEvent(CacheEvent<? extends Object, ? extends Object> event) {
		// TODO Auto-generated method stub
		 log.info("Cache event {} for item with key {}. Old value = {}, New value = {}", event.getType(), event.getKey(), event.getOldValue(), event.getNewValue());
	}

}
