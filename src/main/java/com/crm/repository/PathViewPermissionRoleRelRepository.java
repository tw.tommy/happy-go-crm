package com.crm.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.PathViewPermissionRoleRel;
import com.crm.model.entity.PathViewPermissionRoleRelPk;



@Repository
public interface PathViewPermissionRoleRelRepository extends JpaRepository<PathViewPermissionRoleRel, PathViewPermissionRoleRelPk>{
	
	@Query(value = "SELECT CASE VIEW_PERMISSION WHEN 'NONE' THEN PATH ELSE PATH || '&' || VIEW_PERMISSION END  FROM PATH_VIEW_PERMISSION_ROLE_REL WHERE ROLE =:role",nativeQuery = true)
	public List<String> getIdByRole(@Param("role") String role);
	
	@Query(value = "SELECT DISTINCT PATH,VIEW_PERMISSION FROM PATH_VIEW_PERMISSION_ROLE_REL WHERE VIEW_PERMISSION!='NONE' AND ROLE IN (:roles)" ,nativeQuery = true)
	public List<Map<String,Object>> getAllLegalFunc(@Param("roles") List<String> roles);
}

