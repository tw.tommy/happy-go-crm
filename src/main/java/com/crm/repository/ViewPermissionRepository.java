package com.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.ViewPermission;

@Repository
public interface ViewPermissionRepository extends JpaRepository<ViewPermission, String> {

}
