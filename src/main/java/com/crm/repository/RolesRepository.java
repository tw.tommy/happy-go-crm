package com.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.jwt.model.Roles;



@Repository("rolesRepositoryS")
public interface RolesRepository extends JpaRepository<Roles, String> {

	@Query(value="SELECT ROLE FROM ROLES WHERE USERNAME = :username",nativeQuery = true)
	public List<String> findRoleByUsername(@Param("username") String username);
	
}
