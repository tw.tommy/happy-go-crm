package com.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.RoleSetting;

@Repository("rolesRepository")
public interface RoleSettingRepository extends JpaRepository<RoleSetting, String> {

	@Query
	public List<RoleSetting> findByEnabledTrue();
	
	@Query
	public List<RoleSetting> findByEnabledFalse();
	
	 
}
