package com.crm.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crm.model.entity.PathSetting;

@Repository
public interface PathSettingRepository extends JpaRepository<PathSetting, String>,JpaSpecificationExecutor<PathSetting>{

	@Query(value="SELECT * FROM PATH_SETTING WHERE TIER=0  order BY SORT " , nativeQuery = true)
	public List<PathSetting> findAllOneTier();
	
	@Query(value="SELECT * FROM PATH_SETTING WHERE PARENT =:parent and TIER=:tier order BY SORT " , nativeQuery = true)
	public List<PathSetting> findAllByParentAndTier(@Param("parent") String parent,@Param("tier") int tier);
	
	@Query(value = "SELECT * FROM (SELECT D.ID AS ID,A.PATH || '/'|| B.PATH ||  '/'|| C.PATH || '/'|| D.PATH  AS PATH ,A.TITLE || '/'|| B.TITLE ||  '/'|| C.TITLE ||  '/'|| D.TITLE AS TITLE FROM (\r\n" + 
			"(SELECT * FROM PATH_SETTING WHERE TIER=0 ORDER BY SORT) \r\n" + 
			") A \r\n" + 
			"LEFT JOIN (SELECT * FROM PATH_SETTING WHERE TIER=1 ORDER BY SORT) B ON A.ID = B.PARENT  \r\n" + 
			"LEFT JOIN (SELECT * FROM PATH_SETTING WHERE TIER=2 ORDER BY SORT) C ON B.ID = C.PARENT \r\n" + 
			"LEFT JOIN (SELECT * FROM PATH_SETTING WHERE TIER=3 ORDER BY SORT) D ON c.ID = D.PARENT )",nativeQuery = true)
	public List<Map<String,Object>> getFullPathAndTitle();
	
	@Query(value="SELECT DISTINCT PATH_SETTING.* FROM PATH_SETTING  LEFT JOIN PATH_VIEW_PERMISSION_ROLE_REL ON PATH_SETTING.ID = PATH_VIEW_PERMISSION_ROLE_REL.PATH WHERE PATH_VIEW_PERMISSION_ROLE_REL.VIEW_PERMISSION='NONE' AND  TIER=0 AND EXISTS(SELECT null FROM ROLE_SETTING WHERE ROLE_SETTING.ROLE = PATH_VIEW_PERMISSION_ROLE_REL.ROLE AND ENABLED = 1) AND PATH_VIEW_PERMISSION_ROLE_REL.ROLE IN(:roles)  order BY SORT" , nativeQuery = true)
	public List<PathSetting> findAllOneTierByRole(@Param("roles") List<String> roles);
	
	@Query(value="SELECT DISTINCT PATH_SETTING.* FROM PATH_SETTING LEFT JOIN PATH_VIEW_PERMISSION_ROLE_REL ON PATH_SETTING.ID = PATH_VIEW_PERMISSION_ROLE_REL.PATH WHERE PATH_VIEW_PERMISSION_ROLE_REL.VIEW_PERMISSION='NONE' AND EXISTS(SELECT null FROM ROLE_SETTING WHERE ROLE_SETTING.ROLE = PATH_VIEW_PERMISSION_ROLE_REL.ROLE AND ENABLED = 1) AND PATH_VIEW_PERMISSION_ROLE_REL.ROLE IN(:roles) AND PARENT =:parent and TIER=:tier order BY SORT", nativeQuery = true)
	public List<PathSetting> findAllByParentAndRole(@Param("roles") List<String> roles,@Param("parent") String parent,@Param("tier") int tier);

	@Modifying
	@Query(nativeQuery = true , value = "UPDATE　PATH_SETTING　SET REDIRECT=:redirect WHERE ID=:id")
	public void updateRedirect(@Param("redirect") String redirect ,@Param("id") String id);
}
