package com.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.ApiPermissionSetting;
import com.crm.model.entity.ApiPermissionSettingPk;

@Repository
public interface ApiPermissionSettingRepository extends JpaRepository<ApiPermissionSetting, ApiPermissionSettingPk> {

	@Query(nativeQuery = true , value = "SELECT API FROM API_PERMISSION_SETTING WHERE ROLE=:role")
	public List<String> getCheckApi(@Param("role") String role);
	
	@Modifying
	@Query(nativeQuery = true , value = "DELETE API_PERMISSION_SETTING WHERE ROLE=:role")
	public void deleteByRole(@Param("role") String role);
	
	@Query(nativeQuery = true , value = "SELECT PATH , ROLE FROM API_PERMISSION_SETTING WHERE EXISTS(SELECT NULL FROM ROLE_SETTING WHERE ROLE_SETTING.ROLE = API_PERMISSION_SETTING.ROLE AND ENABLED = 1)")
	public List<ApiPermissionSetting> findAllEnableRole();
	
}
