package com.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.TokenRecord;

@Repository
public interface TokenRecordRepository extends JpaRepository<TokenRecord, String> {

	@Modifying
	@Query(nativeQuery = true , value = "UPDATE TOKEN_RECORD SET EXPRIED_DATE = :expriedDate WHERE USERNAME = :username")
	public void update(@Param("expriedDate") String expriedDate , @Param("username") String username);
	
	@Modifying
	@Query(nativeQuery = true , value = "DELETE TOKEN_RECORD  WHERE USERNAME = :username")
	public void deleteByUsername(@Param("username") String username);
	
}
