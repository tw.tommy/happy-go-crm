package com.crm.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.crm.model.entity.PathViewPermissionRel;
import com.crm.model.entity.PathViewPermissionRelPk;

@Repository
public interface PathViewPermissionRelRepositroy extends JpaRepository<PathViewPermissionRel, PathViewPermissionRelPk> {

	@Query(value = "SELECT PATH_VIEW_PERMISSION_REL.VIEW_PERMISSION,PATH_VIEW_PERMISSION_REL.PATH , VIEW_PERMISSION.NAME FROM PATH_VIEW_PERMISSION_REL LEFT JOIN VIEW_PERMISSION ON PATH_VIEW_PERMISSION_REL.VIEW_PERMISSION = VIEW_PERMISSION.VIEW_PERMISSION WHERE PATH_VIEW_PERMISSION_REL.PATH = :path" , nativeQuery = true)
	public List<Map<String,Object>> findByPath(@Param("path")String path);
	
	@Modifying
	@Query(value="DELETE PATH_VIEW_PERMISSION_REL　WHERE PATH = :path",nativeQuery = true)
	public void delete(@Param("path") String path);
	
	@Query(value="SELECT VIEW_PERMISSION FROM PATH_VIEW_PERMISSION_REL　WHERE PATH = :path",nativeQuery = true)
	public List<String> findviewPermissionByPath(@Param("path") String path);
}
