package com.crm.controllerAdvice;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.crm.common.AttemptException;
import com.crm.common.HttpStatus;
import com.crm.common.PasswordAndReNotMatchException;
import com.crm.common.Result;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ExceptionAdvice {

	@ExceptionHandler(value = IllegalArgumentException.class)
	public Result<String> errorHandle(IllegalArgumentException ex) {
		StringWriter stack = new StringWriter();
		ex.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
		return new Result<String>(HttpStatus.EXCEPTION, ex.getLocalizedMessage());
	}

	@ExceptionHandler(value = Exception.class)
	public Result<String> errorHandle(Exception ex) {
		StringWriter stack = new StringWriter();
		ex.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
		if(StringUtils.isBlank(ex.getLocalizedMessage())) {
			return new Result<String>(HttpStatus.EXCEPTION, "系統發生錯誤，請聯絡系統管理者!");
		}
		return new Result<String>(HttpStatus.EXCEPTION, ex.getLocalizedMessage());
	}

	@ExceptionHandler(value = BadCredentialsException.class)
	public Result<String> errorHandle(BadCredentialsException ex) {
		StringWriter stack = new StringWriter();
		ex.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
		return new Result<String>(HttpStatus.EXCEPTION, ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(value = AttemptException.class)
	public Result<String> errorHandle(AttemptException ex) {
		StringWriter stack = new StringWriter();
		ex.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
		return new Result<String>(HttpStatus.EXCEPTION, "LOGIN_ATTEMPT",ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(value = PasswordAndReNotMatchException.class)
	public Result<String> errorHandle(PasswordAndReNotMatchException ex) {
		StringWriter stack = new StringWriter();
		ex.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
		return new Result<String>(HttpStatus.EXCEPTION, ex.getLocalizedMessage());
	}
	

	/*@ExceptionHandler(value = DisabledException.class)
	public Result<String> errorHandle(DisabledException ex){
		log.info("DisabledException = " + ex.getLocalizedMessage());
		log.info("DisabledException = " + ex.getMessage());
		//ex.printStackTrace();
		return new Result<String>(HttpStatus.exception,ex.getLocalizedMessage());	
	}
	
	@ExceptionHandler(value = AccessDeniedException.class)
	public Result<String> errorHandle(AccessDeniedException ex){
		log.info("AccessDeniedException" + ex.getLocalizedMessage());
		log.info("AccessDeniedException = " + ex.getMessage());
		//ex.printStackTrace();
		return new Result<String>(HttpStatus.exception,"權限不足!");	
	}
	
	@ExceptionHandler(value = JwtException.class)
	public Result<String> errorHandle(JwtException ex){
		log.info("ExpiredJwtException" + ex.getLocalizedMessage());
		log.info("ExpiredJwtException = " + ex.getMessage());
		//ex.printStackTrace();
		return new Result<String>(HttpStatus.exception,"Token已過期!");	
	}*/
}
