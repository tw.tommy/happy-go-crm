package com.crm.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

import com.crm.model.entity.RoleSetting;

import lombok.extern.slf4j.Slf4j;

@Converter
@Slf4j
public class RoleConverter implements AttributeConverter<String, String> {
	
	@Override
	public String convertToDatabaseColumn(String attribute) {
		return attribute;
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		if(StringUtils.isBlank(dbData)) {
			return dbData;
		}
		if("ADMIN".equals(dbData)) {
			return "管理者";
		}else if("USER".equals(dbData)) {
			return "用戶";
		}
			
		return dbData;
	}

 

}
