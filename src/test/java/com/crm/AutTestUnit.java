package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.cli.CliDocumentation.curlRequest;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.junit.runners.MethodSorters;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@FixMethodOrder(MethodSorters.JVM)
public class AutTestUnit {

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

	private MockMvc mockMvc;

	private String token;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
				.apply(documentationConfiguration(this.restDocumentation).snippets().withDefaults(curlRequest(),
						PayloadDocumentation.requestBody(), PayloadDocumentation.responseBody()))
				.build();
		
		String username = "admin";
		String password = "123";
		
		JSONObject jwtRequest = new JSONObject();
		jwtRequest.put("username", username);
		jwtRequest.put("password", password);
		MvcResult result = mockMvc
				.perform(post("/authenticate").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(jwtRequest.toString()).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK")))
				.andDo(document("AUT/authenticate",
						requestFields(fieldWithPath("username").description("*使用者帳號"),
								fieldWithPath("password").description("*使用者密碼")),
						responseFields(fieldWithPath("status").description("狀態"),
								fieldWithPath("message").description("訊息"), fieldWithPath("data").description("token"),
								fieldWithPath("content").description("-"))))
				.andReturn();

		token = result.getResponse().getContentAsString();

		JSONObject jsonObject = new JSONObject(token);
		token = "Bearer " + jsonObject.getString("data");
		log.info("token  " + token);

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getUserInfo() throws Exception {
		this.mockMvc
				.perform(get("/getUserInfo")
						.header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK")))
				.andDo(document("AUT/getUserInfo",
						responseFields(fieldWithPath("status").description("狀態"),
								fieldWithPath("message").description("訊息"), fieldWithPath("data.name").description("使用者ID"),
								fieldWithPath("data.roles").description("使用者權限"),
								fieldWithPath("data.avatar").description("使用者人頭"),
								fieldWithPath("data.legalFunc[].FUNC").description("驗證畫面權限"),
								fieldWithPath("data.legalFunc[].PATH").description("需要驗證畫面權限的路徑"),
								fieldWithPath("content").description("-"))));

	}
	
	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void logout() throws Exception {
		this.mockMvc
				.perform(post("/user/logout")
						.header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK")))
				.andDo(document("AUT/logout",
						responseFields(fieldWithPath("status").description("狀態"),
								fieldWithPath("message").description("訊息"),
								fieldWithPath("data").description("-"),
								fieldWithPath("content").description("-"))));

	}

}
