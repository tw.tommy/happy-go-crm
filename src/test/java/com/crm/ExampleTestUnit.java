package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.cli.CliDocumentation.curlRequest;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.crm.model.entity.Notification;
import com.crm.restController.admin.user.UserRegisterForm;
import com.crm.restController.usersetting.Form;

import org.junit.runners.MethodSorters;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@FixMethodOrder(MethodSorters.JVM)
public class ExampleTestUnit extends MvcMockBase {

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void email() throws Exception {
		this.mockMvc.perform(post("/example/email/function").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("EXAMPLE/email", responseFields(this.responseFieldsDef)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void notification() throws Exception {

		Notification notification = new Notification();
		notification.setTitle("JunitTest");
		notification.setBulletin("測試用!");

		FieldDescriptor[] request = { fieldWithPath("title").description("*標題"),
				fieldWithPath("bulletin").description("*內容"), fieldWithPath("id").description("*ID內容").ignored(),
				fieldWithPath("creater").description("*創作者").ignored(),
				fieldWithPath("createDate").description("*創作日期").ignored() };

		this.mockMvc
				.perform(post("/example/notification/function").header("Authorization", token)
						.content(this.convertString(notification)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("status", is("OK"))).andExpect(status().isOk()).andDo(document(
						"EXAMPLE/notification", requestFields(request), responseFields(this.responseFieldsDef)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getBulletin() throws Exception {

		ParameterDescriptor[] parameterDescriptor = { parameterWithName("page").description("頁") };

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].id").description("路徑ID"),
				fieldWithPath("data[].name").description("路徑NAME"), fieldWithPath("data[].path").description("路徑"),
				fieldWithPath("data[].title").description("目錄標題"), fieldWithPath("data[].icon").description("ICON"),
				fieldWithPath("data[].tier").description("階層層級"),
				fieldWithPath("data[].component").description("前端畫面元件"),
				fieldWithPath("data[].sort").description("路徑排序"),
				fieldWithPath("data[].redirect").description("目錄按下後顯示的畫面"),
				fieldWithPath("data[].parent").description("目錄上層"),
				fieldWithPath("data[].hidden").description("目錄是否顯示"), fieldWithPath("content").description("-"),
				fieldWithPath("content[].id").description("-").ignored(),
				fieldWithPath("content[].tier").description("-").ignored(),
				fieldWithPath("content[].name").description("-").ignored(),
				fieldWithPath("content[].path").description("-").ignored(),
				fieldWithPath("content[].title").description("-").ignored(),
				fieldWithPath("content[].icon").description("-").ignored(),
				fieldWithPath("content[].component").description("-").ignored(),
				fieldWithPath("content[].sort").description("-").ignored(),
				fieldWithPath("content[].redirect").description("-").ignored(),
				fieldWithPath("content[].parent").description("-").ignored(),
				fieldWithPath("content[].hidden").description("-").ignored() };

		mockMvc.perform(get("/example/notification/function").header("Authorization", token).param("page", "1")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("EXAMPLE/getBulletin", requestParameters(parameterDescriptor)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void pdf() throws Exception {

		ParameterDescriptor[] parameterDescriptor = { parameterWithName("fileType").description("報表類型") };

		mockMvc.perform(get("/example/report/function").header("Authorization", token).param("fileType", "PDF")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_OCTET_STREAM_VALUE))
				.andExpect(status().isOk()).andDo(document("EXAMPLE/report", requestParameters(parameterDescriptor)));
	}

}
