package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.request.ParameterDescriptor;

import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import com.crm.restController.admin.path.SettingUpdateForm;
import com.crm.restController.admin.path.SettingUpdateRedirectForm;
import com.crm.restController.admin.role.InsertForm;
import com.crm.restController.api.PathForm;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@FixMethodOrder(MethodSorters.JVM)
public class RoleTestUnit extends MvcMockBase {

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void add() throws Exception {
		InsertForm form = new InsertForm();
		form.setDef(null);
		form.setEnabled(true);
		form.setName("管理者");
		form.setRole("ADMIN");

		FieldDescriptor[] request = { fieldWithPath("def").description("敘述"),
				fieldWithPath("enabled").description("*狀態"), fieldWithPath("name").description("*角色名稱"),
				fieldWithPath("role").description("*角色") };

		this.postRequest("/admin/role/insert/index/add", "ROLE/add", form, request, null, this.responseFieldsDef,
				null);
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void query() throws Exception {

		ParameterDescriptor[] parameterDescriptor = {
				parameterWithName("enable").description("權限狀態，有ALL,true,false三種值") };

		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("content").description("-"),
				fieldWithPath("data[].role").description("角色"), fieldWithPath("data[].name").description("角色名稱"),
				fieldWithPath("data[].id").description("相關權限"), fieldWithPath("data[].roleName").description("角色名稱"),
				fieldWithPath("data[].def").description("角色名稱").optional(),
				fieldWithPath("data[].enabled").description("角色狀態"),

				fieldWithPath("content[].role").description("角色").ignored(), 
				fieldWithPath("content[].name").description("角色名稱").ignored(),
				fieldWithPath("content[].id").description("相關權限").ignored(),
				fieldWithPath("content[].roleName").description("角色名稱").ignored(),
				fieldWithPath("content[].def").description("角色名稱").optional().ignored(),
				fieldWithPath("content[].enabled").description("角色狀態").ignored()

		};

		mockMvc.perform(get("/admin/role/query/index/query").header("Authorization", token).param("enable", "ALL")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("ROLE/query", requestParameters(parameterDescriptor), responseFields(response)));

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void findAll() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("content").description("-"),

				fieldWithPath("data[].role").description("角色"), fieldWithPath("data[].name").description("角色名稱"),
				fieldWithPath("data[].id").description("-").optional(),
				fieldWithPath("data[].roleName").description("角色名稱"),
				fieldWithPath("data[].def").description("角色名稱").optional(),
				fieldWithPath("data[].enabled").description("角色狀態"),

				fieldWithPath("content[].role").description("角色"), fieldWithPath("content[].name").description("角色名稱"),
				fieldWithPath("content[].id").description("-").optional(),
				fieldWithPath("content[].roleName").description("角色名稱"),
				fieldWithPath("content[].def").description("角色名稱").optional(),
				fieldWithPath("content[].enabled").description("角色狀態")

		};

		mockMvc.perform(get("/setting/role/findAll").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("ROLE/findAll", responseFields(response)));

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void update() throws Exception {
		InsertForm form = new InsertForm();
		form.setDef(null);
		form.setEnabled(true);
		form.setName("管理者");
		form.setRole("ADMIN");
		FieldDescriptor[] request = { fieldWithPath("def").description("敘述"),
				fieldWithPath("enabled").description("*狀態"), fieldWithPath("name").description("*角色名稱"),
				fieldWithPath("role").description("*角色") };
		this.postRequest("/admin/role/query/index/update", "ROLE/update", form, request, null,
				this.responseFieldsDef, null);
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void deleteRole() throws Exception {
		ParameterDescriptor[] parameterDescriptor = { parameterWithName("role").description("角色ID") };
		mockMvc.perform(delete("/admin/role/query/index/delete").header("Authorization", token).param("role", "ADMIN")
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK"))).andDo(document("ROLE/delete",
						requestParameters(parameterDescriptor), responseFields(this.responseFieldsDef)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void report() throws Exception {
		ParameterDescriptor[] parameterDescriptor = {
				parameterWithName("enable").description("角色狀態，有ALL,true,false三種值") };
		mockMvc.perform(get("/admin/role/query/index/report").header("Authorization", token).param("enable", "ALL")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_OCTET_STREAM_VALUE))
				.andExpect(status().isOk()).andDo(document("ROLE/report", requestParameters(parameterDescriptor)));

	}

}
