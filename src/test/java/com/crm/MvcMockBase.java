package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.cli.CliDocumentation.curlRequest;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MvcMockBase {

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

	public MockMvc mockMvc;

	// 驗證Token
	public String token;

	// 預設回應參數
	public FieldDescriptor[] responseFieldsDef = { fieldWithPath("status").description("狀態"),
			fieldWithPath("message").description("訊息"), fieldWithPath("data").description("-"),
			fieldWithPath("content").description("-") };

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
				.apply(documentationConfiguration(this.restDocumentation).snippets().withDefaults(curlRequest(),
						PayloadDocumentation.requestBody(), PayloadDocumentation.responseBody()))
				.build();
		String username = "admin";
		String password = "123";
		JSONObject jwtRequest = new JSONObject();
		jwtRequest.put("username", username);
		jwtRequest.put("password", password);
		MvcResult result = mockMvc
				.perform(post("/authenticate").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(jwtRequest.toString()).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK"))).andReturn();
		token = result.getResponse().getContentAsString();
		JSONObject jsonObject = new JSONObject(token);
		token = "Bearer " + jsonObject.getString("data");
		log.info("token  " + token);
	}

	// 物件轉STRING
	public String convertString(Object ob) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(ob);
	}

	/*
	 * POST TEST
	 * 
	 * @PARAM uri 路徑
	 * 
	 * @PARAM docName 文件名稱
	 * 
	 * @PARAM content post body
	 * 
	 * @PARAM resquestField 請求參數
	 * 
	 * @PARAM resquestDesc 請求參數敘述
	 * 
	 * @PARAM responseField 回應參數
	 * 
	 * @PARAM responseDesc 回應參數敘述
	 */
	public void postRequest(String uri, String docName, Object content, FieldDescriptor[] resquest,
			FieldDescriptor[] resquestArr, FieldDescriptor[] response, FieldDescriptor[] responseArr) throws Exception {

		if (resquest == null && resquestArr == null && responseArr == null) {
			mockMvc.perform(post(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(response))).andReturn();
			return;
		}

		if (resquest == null && resquestArr == null && responseArr != null) {
			mockMvc.perform(post(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(responseArr).andWithPrefix("[].", response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr == null && responseArr == null) {
			mockMvc.perform(post(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquest), responseFields(response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr == null) {
			mockMvc.perform(post(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK"))).andDo(document(docName,
							requestFields(resquestArr).andWithPrefix("[].", resquest), responseFields(response)))
					.andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr != null) {
			mockMvc.perform(post(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquestArr).andWithPrefix("[].", resquest),
							responseFields(responseArr).andWithPrefix("[].", response)))
					.andReturn();
			return;
		}

	}

	/*
	 * delete TEST
	 * 
	 * @PARAM uri 路徑
	 * 
	 * @PARAM docName 文件名稱
	 * 
	 * @PARAM content post body
	 * 
	 * @PARAM resquestField 請求參數
	 * 
	 * @PARAM resquestDesc 請求參數敘述
	 * 
	 * @PARAM responseField 回應參數
	 * 
	 * @PARAM responseDesc 回應參數敘述
	 */
	public void deleteRequest(String uri, String docName, Object content, FieldDescriptor[] resquest,
			FieldDescriptor[] resquestArr, FieldDescriptor[] response, FieldDescriptor[] responseArr) throws Exception {


		if (resquest == null && resquestArr == null && responseArr == null) {
			mockMvc.perform(delete(uri).header("Authorization", token).content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(response))).andReturn();
			return;
		}

		if (resquest == null && resquestArr == null && responseArr != null) {
			mockMvc.perform(delete(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(responseArr).andWithPrefix("[].", response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr == null && responseArr == null) {
			mockMvc.perform(delete(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquest), responseFields(response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr == null) {
			mockMvc.perform(delete(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK"))).andDo(document(docName,
							requestFields(resquestArr).andWithPrefix("[].", resquest), responseFields(response)))
					.andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr != null) {
			mockMvc.perform(delete(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquestArr).andWithPrefix("[].", resquest),
							responseFields(responseArr).andWithPrefix("[].", response)))
					.andReturn();
			return;
		}

	}
	
	public void deleteRequest(String uri, String docName, Object content, ParameterDescriptor[] resquest, FieldDescriptor[] response, FieldDescriptor[] responseArr) throws Exception {


		if (resquest == null  && responseArr == null) {
			mockMvc.perform(delete(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(response))).andReturn();
			return;
		}

		if (resquest == null && responseArr != null) {
			mockMvc.perform(delete(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(responseArr).andWithPrefix("[].", response))).andReturn();
			return;
		}

		if (resquest != null  && responseArr == null) {
			mockMvc.perform(delete(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, pathParameters(resquest), responseFields(response))).andReturn();
			return;
		}



	}

	/*
	 * get TEST
	 * 
	 * @PARAM uri 路徑
	 * 
	 * @PARAM docName 文件名稱
	 * 
	 * @PARAM content post body
	 * 
	 * @PARAM resquestField 請求參數
	 * 
	 * @PARAM resquestDesc 請求參數敘述
	 * 
	 * @PARAM responseField 回應參數
	 * 
	 * @PARAM responseDesc 回應參數敘述
	 */
	public void getRequest(String uri, String docName, Object content, FieldDescriptor[] resquest,
			FieldDescriptor[] resquestArr, FieldDescriptor[] response, FieldDescriptor[] responseArr) throws Exception {


		if (resquest == null && resquestArr == null && responseArr == null) {
			mockMvc.perform(get(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(response))).andReturn();
			return;
		}

		if (resquest == null && resquestArr == null && responseArr != null) {
			mockMvc.perform(get(uri).header("Authorization", token).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, responseFields(responseArr).andWithPrefix("[].", response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr == null && responseArr == null) {
			mockMvc.perform(get(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquest), responseFields(response))).andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr == null) {
			mockMvc.perform(get(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK"))).andDo(document(docName,
							requestFields(resquestArr).andWithPrefix("[].", resquest), responseFields(response)))
					.andReturn();
			return;
		}

		if (resquest != null && resquestArr != null && responseArr != null) {
			mockMvc.perform(get(uri).header("Authorization", token).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(this.convertString(content)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
					.andDo(document(docName, requestFields(resquestArr).andWithPrefix("[].", resquest),
							responseFields(responseArr).andWithPrefix("[].", response)))
					.andReturn();
			return;
		}

	}

	public FieldDescriptor[] getFieldDescriptorArray(String[] field, String[] desc) throws Exception {

		if (null == field || null == desc) {
			throw new Exception("field or desc is null!!!");
		}

		if (field.length != desc.length) {
			throw new Exception("field and desc's length is not same!!!");
		}

		FieldDescriptor[] fields = new FieldDescriptor[field.length];

		for (int i = 0; i < field.length; i++) {
			fields[i] = subsectionWithPath(field[i]).description(desc[i]);
		}

		return fields;
	}

}
