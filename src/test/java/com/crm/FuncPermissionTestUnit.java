package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;

import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.request.ParameterDescriptor;

import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import com.crm.restController.admin.func.PathFuncAddForm;
import com.crm.restController.admin.path.SettingUpdateForm;
import com.crm.restController.admin.path.SettingUpdateRedirectForm;
import com.crm.restController.admin.permission.SettingForm;
import com.crm.restController.api.PathForm;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@FixMethodOrder(MethodSorters.JVM)
public class FuncPermissionTestUnit extends MvcMockBase {

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void list() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].permission").description("權限ID"),
				fieldWithPath("data[].name").description("權限名稱"),
				fieldWithPath("data[].def").description("權限定義").optional(),
				fieldWithPath("content[].permission").description("-").ignored(),
				fieldWithPath("content[].name").description("-").ignored(),
				fieldWithPath("content[].def").description("-").ignored() };

		mockMvc.perform(get("/setting/funcPermission/list").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("FUNCTION/list", responseFields(response)));

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void add() throws Exception {

		PathFuncAddForm form = new PathFuncAddForm();
		form.setPath("/admin/func/pathFunc/index");
		List<String> permission = new ArrayList<String>();
		permission.add("add");
		permission.add("update");
		form.setViewPermission(permission);
		
		FieldDescriptor[] request = { fieldWithPath("path").description("路徑ID"),
				fieldWithPath("viewPermission[]").description("方法")		
		};
		
		mockMvc.perform(post("/admin/func/pathFunc/index/add").header("Authorization", token)
				.content(this.convertString(form))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("FUNCTION/add-path-function", requestFields(request),responseFields(this.responseFieldsDef)));

	}
	
	
	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void permissionAdd() throws Exception {

		SettingForm form = new SettingForm();
		form.setRole("2222222");
		form.setTitle("/admin");
		form.setPaths(new ArrayList<String>());
		form.setExcludePaths(new ArrayList<String>());
		
		FieldDescriptor[] request = { 
				fieldWithPath("role").description("角色ID"),
				fieldWithPath("title").description("主標題ID"),
				fieldWithPath("paths[]").description("新增權限路徑").optional(),
				fieldWithPath("excludePaths").description("刪除權限路徑").optional()	
		};
		
		mockMvc.perform(post("/admin/permission/setting/index/add").header("Authorization", token)
				.content(this.convertString(form))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("FUNCTION/add-path-permission", requestFields(request),responseFields(this.responseFieldsDef)));

	}

}
