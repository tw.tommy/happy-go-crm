package com.crm;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;

import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.request.ParameterDescriptor;

import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import com.crm.restController.admin.path.SettingUpdateForm;
import com.crm.restController.admin.path.SettingUpdateRedirectForm;
import com.crm.restController.api.PathForm;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@FixMethodOrder(MethodSorters.JVM)
public class PathTestUnit extends MvcMockBase {

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void add() throws Exception {

		PathForm one = new PathForm();
		one.setTitle("範例1");
		one.setPath("example1");
		one.setIcon("example");
		PathForm two = new PathForm();
		two.setTitle("範例2");
		two.setPath("example2");
		two.setIcon("example");
		PathForm three = new PathForm();
		three.setTitle("範例3");
		three.setPath("example3");
		three.setIcon("example");
		PathForm four = new PathForm();
		four.setTitle("範例4");
		four.setPath("example4");
		four.setIcon("example");

		List<PathForm> pathForm = new ArrayList<PathForm>();
		pathForm.add(one);
		pathForm.add(two);
		pathForm.add(three);
		pathForm.add(four);

		FieldDescriptor[] request = { fieldWithPath("[].title").description("*標題"),
				fieldWithPath("[].path").description("*路徑"), fieldWithPath("[].icon").description("*圖標") };

		this.postRequest("/admin/path/setting/index/add", "PATH/add", pathForm, request, null,
				this.responseFieldsDef, null);

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void update() throws Exception {
		SettingUpdateForm settingUpdateForm = new SettingUpdateForm();
		settingUpdateForm.setId("example1");
		settingUpdateForm.setTitle("修改後的example1");
		settingUpdateForm.setIcon("example");

		FieldDescriptor[] request = { fieldWithPath("id").description("*路徑ID"),
				fieldWithPath("title").description("*路徑標題"), fieldWithPath("icon").description("*路徑圖標") };

		this.postRequest("/admin/path/setting/index/update", "PATH/update", settingUpdateForm, request, null,
				this.responseFieldsDef, null);

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void updateRedirect() throws Exception {
		SettingUpdateRedirectForm settingUpdateRedirectForm = new SettingUpdateRedirectForm();
		settingUpdateRedirectForm.setId("/example1/example2");
		settingUpdateRedirectForm.setRedirect("/example1/example2/example3/example4");
		FieldDescriptor[] request = { fieldWithPath("id").description("*路徑ID"),
				fieldWithPath("redirect").description("*次目錄點下後顯示的畫面") };
		this.postRequest("/admin/path/setting/index/updateRedirect", "PATH/updateRedirect",
				settingUpdateRedirectForm, request, null, this.responseFieldsDef, null);

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void updateSort() throws Exception {
		List<String> ids = new ArrayList<String>();
		ids.add("/example1/example2/example3/example5");
		ids.add("/example1/example2/example3/example4");

		FieldDescriptor[] request = { fieldWithPath("[].").description("路徑ID") };
	
		mockMvc.perform(post("/admin/path/setting/index/updateSort").header("Authorization", token)
				.content(this.convertString(ids))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/updateSort",requestFields(request) ,responseFields(this.responseFieldsDef) ));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void delete() throws Exception {

		String id = "/example1/example2/example3/example6";

		ParameterDescriptor[] req = { parameterWithName("").description("*路徑ID") };

		this.deleteRequest("/admin/path/setting/index/delete", "PATH/delete", id, null, null,
				this.responseFieldsDef, null);

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void url() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data").description("前端路徑"),
				fieldWithPath("content").description("-") };

		mockMvc.perform(get("/setting/path/url").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/url", responseFields(response)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getAllPath() throws Exception {
		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data").description("所有的前端路徑"),
				fieldWithPath("content").description("-") };

		mockMvc.perform(get("/setting/path/getAllPath").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/getAllPath", responseFields(response)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getAllPathParam() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data").description("所有的前端路徑"),
				fieldWithPath("content").description("-") };
		String param = "/admin";
		mockMvc.perform(get("/setting/path/getAllPath", param).header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/getAllPath-Param", responseFields(response)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getAllPathAndTitle() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].ID").description("路徑ID"),
				fieldWithPath("data[].PATH").description("前端畫面路徑"), fieldWithPath("data[].TITLE").description("標題"),
				fieldWithPath("content").description("-") };

		mockMvc.perform(get("/setting/path/getAllPathAndTitle").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/getAllPathAndTitle"));

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getOneTier() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].id").description("路徑ID"),
				fieldWithPath("data[].name").description("路徑NAME"), fieldWithPath("data[].path").description("路徑"),
				fieldWithPath("data[].title").description("目錄標題"), fieldWithPath("data[].icon").description("ICON"),
				fieldWithPath("data[].tier").description("階層層級"),
				fieldWithPath("data[].component").description("前端畫面元件"),
				fieldWithPath("data[].sort").description("路徑排序"),
				fieldWithPath("data[].redirect").description("目錄按下後顯示的畫面"),
				fieldWithPath("data[].parent").description("目錄上層"),
				fieldWithPath("data[].hidden").description("目錄是否顯示"), fieldWithPath("content").description("-"),
				fieldWithPath("content[].id").description("-").ignored(),
				fieldWithPath("content[].tier").description("-").ignored(),
				fieldWithPath("content[].name").description("-").ignored(),
				fieldWithPath("content[].path").description("-").ignored(),
				fieldWithPath("content[].title").description("-").ignored(),
				fieldWithPath("content[].icon").description("-").ignored(),
				fieldWithPath("content[].component").description("-").ignored(),
				fieldWithPath("content[].sort").description("-").ignored(),
				fieldWithPath("content[].redirect").description("-").ignored(),
				fieldWithPath("content[].parent").description("-").ignored(),
				fieldWithPath("content[].hidden").description("-").ignored() };

		mockMvc.perform(get("/setting/path/getOneTier").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/getOneTier", responseFields(response)));

	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getTree() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data").description("依據第一階層路徑ID找出其下面的路徑"),
				fieldWithPath("content").description("-") };

		ParameterDescriptor[] parameterDescriptor = { parameterWithName("id").description("路徑ID") };

		mockMvc.perform(get("/setting/path/getTree").header("Authorization", token).param("id", "/admin")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is("OK")))
				.andDo(document("PATH/getTree", requestParameters(parameterDescriptor), responseFields(response)));
	}

	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getMenuByParent() throws Exception {

		ParameterDescriptor[] parameterDescriptor = { parameterWithName("parent").description("父層路徑"),
				parameterWithName("tier").description("階層") };

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].id").description("路徑ID"),
				fieldWithPath("data[].name").description("路徑NAME"), fieldWithPath("data[].path").description("路徑"),
				fieldWithPath("data[].title").description("目錄標題"), fieldWithPath("data[].icon").description("ICON"),
				fieldWithPath("data[].tier").description("階層層級"),
				fieldWithPath("data[].component").description("前端畫面元件"),
				fieldWithPath("data[].sort").description("路徑排序"),
				fieldWithPath("data[].redirect").description("目錄按下後顯示的畫面"),
				fieldWithPath("data[].parent").description("目錄上層"),
				fieldWithPath("data[].hidden").description("目錄是否顯示"), fieldWithPath("content").description("-"),
				fieldWithPath("content[].id").description("-").ignored(),
				fieldWithPath("content[].tier").description("-").ignored(),
				fieldWithPath("content[].name").description("-").ignored(),
				fieldWithPath("content[].path").description("-").ignored(),
				fieldWithPath("content[].title").description("-").ignored(),
				fieldWithPath("content[].icon").description("-").ignored(),
				fieldWithPath("content[].component").description("-").ignored(),
				fieldWithPath("content[].sort").description("-").ignored(),
				fieldWithPath("content[].redirect").description("-").ignored(),
				fieldWithPath("content[].parent").description("-").ignored(),
				fieldWithPath("content[].hidden").description("-").ignored() };
		
		mockMvc.perform(get("/setting/path/getMenuByParent").header("Authorization", token).param("parent", "/admin")
				.param("tier", "1").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK"))).andDo(document("PATH/getMenuByParent",
						requestParameters(parameterDescriptor), responseFields(response)));

	}
	
	
	@WithMockUser(username = "admin", roles = { "ADMIN", "USER", "BASIC" })
	@Test
	public void getPathByParent() throws Exception {

		FieldDescriptor[] response = { fieldWithPath("status").description("*狀態"),
				fieldWithPath("message").description("訊息"), fieldWithPath("data[].id").description("路徑ID"),
				fieldWithPath("data[].name").description("路徑NAME"), fieldWithPath("data[].path").description("路徑"),
				fieldWithPath("data[].title").description("目錄標題"), fieldWithPath("data[].icon").description("ICON"),
				fieldWithPath("data[].tier").description("階層層級"),
				fieldWithPath("data[].component").description("前端畫面元件"),
				fieldWithPath("data[].sort").description("路徑排序"),
				fieldWithPath("data[].redirect").description("目錄按下後顯示的畫面"),
				fieldWithPath("data[].parent").description("目錄上層"),
				fieldWithPath("data[].hidden").description("目錄是否顯示"), fieldWithPath("content").description("-"),
				fieldWithPath("content[].id").description("目錄上層").ignored(),
				fieldWithPath("content[].tier").description("目錄上層").ignored(),
				fieldWithPath("content[].name").description("目錄上層").ignored(),
				fieldWithPath("content[].path").description("目錄上層").ignored(),
				fieldWithPath("content[].title").description("目錄上層").ignored(),
				fieldWithPath("content[].icon").description("目錄上層").ignored(),
				fieldWithPath("content[].component").description("目錄上層").ignored(),
				fieldWithPath("content[].sort").description("目錄上層").ignored(),
				fieldWithPath("content[].redirect").description("目錄上層").ignored(),
				fieldWithPath("content[].parent").description("目錄上層").ignored(),
				fieldWithPath("content[].hidden").description("目錄上層").ignored() };
		
		mockMvc.perform(get("/setting/path/getPathByParent").header("Authorization", token)
				.param("tier", "1").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is("OK"))).andDo(document("PATH/getPathByParent",
						 responseFields(response)));

	}

}
